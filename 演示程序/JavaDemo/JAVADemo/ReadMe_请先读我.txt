﻿【简介】

1、此demo是Win32 Java Demo，主要实现实时预览、云台、智能交通、下载录像。


【注意事项】

1、运行
	
   1) NetSDKLib.java里设置dll动态库的路径读取 config.properties 配置文件

  （在运行前，必须保证dll动态库拷贝到libs目录下） 

   双击run.bat运行(如果运行失败，请修改脚本里的jre路径)

   2) run.bat运行的是/target//netsdk-1.0-demo.jar

   run.bat 脚本中指定了该demo的运行环境：jre为1.6, 需要设置JVM大小： -Xms256m -Xmx512m

2、动态库目录
   JNADemo\libs  （包含动态库以及第三方jar）  

3、配置文件
   .classpath    .project   pom.xml打jar包脚本 

4、工程IDE
   eclipse，字符编码设置（UTF-8）,

5、不建议在SDK的回调函数中处理数据，一般需要将回调数据拷贝出来，并在用户自己开启的线程中处理回调出来的数据。

6. 运行有问题，请到大华官网下载最新的网络SDK版本：http://www.dahuatech.com/index.php/service/downloadlists/836.html 替换程序中的库文件。


【Demo Features】
1.Demo live preview , ptz control, intelligent traffic, download record function.this demo must run on Windows Operate System.

【NOTE】
1. Running Program
   before running program you should be sure to copy dhnetsdk's DLL from config.properties and have installed jre1.6 or high,then you just double click the run.bat script.
   when you open run.bat with editor ,you will find the specified jre path , and parameter config of jvm which the intelligent traffic application need.if you jre path is not in
   the default directory ,you should modify run.bat script: "call set path=you jre pat;%path%;". 

2. DLL Directory:
   JNADemo\libs(included dhnetsdk's win32 dll and third-party jar package)

3.Config files
  .classpath    .project   pom.xml

4.IDE of project 
  eclipse，and all java source file was saved as utf-8.

5.Deal with data on the callback body is no recommendation, you'd better copy the data from the callback and handle it on other thread which you opened .
   
6.If run the program with some problems,please go to 
http://www.dahuasecurity.com/download_3.html download the newest version,and replace the DLL files.
