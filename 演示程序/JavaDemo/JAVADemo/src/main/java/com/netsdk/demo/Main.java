package main.java.com.netsdk.demo;

import javax.swing.SwingUtilities;

import main.java.com.netsdk.demo.frame.MainFrame;
import main.java.com.netsdk.lib.NetSDKLib;


public class Main {  
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				if(NetSDKLib.NETSDK_INSTANCE != null
					&& NetSDKLib.CONFIG_INSTANCE != null) {
					MainFrame demo = new MainFrame();
					demo.setVisible(true);
				}
			}
		});	
	}
}