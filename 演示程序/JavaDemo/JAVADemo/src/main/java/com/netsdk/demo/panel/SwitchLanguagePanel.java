package main.java.com.netsdk.demo.panel;

import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import main.java.com.netsdk.demo.frame.FunctionFrame;
import main.java.com.netsdk.common.Res;
import main.java.com.netsdk.common.Res.LanguageType;

/*
 * 切换语言面板
 */
public class SwitchLanguagePanel extends JPanel {
	private static final long serialVersionUID = 1L;
	
	public SwitchLanguagePanel(final Frame frame) {
		setLayout(new FlowLayout());
		setBorder(new EmptyBorder(50, 0, 0, 0));
		
		String[] CnEn = {"简体中文", "English"};
		jComboBox = new JComboBox(CnEn);	
		
		nextButton = new JButton("下一步");

		add(jComboBox);
		add(nextButton); 
	    
		jComboBox.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent arg0) {
				LanguageType type = jComboBox.getSelectedIndex() == 0 ? LanguageType.Chinese : LanguageType.English;
				Res.string().switchLanguage(type);
				
				if(jComboBox.getSelectedIndex() == 0) {
					nextButton.setText("下一步");
				} else {
					nextButton.setText("next");
				}
			}
		});
		
	    nextButton.addActionListener(new ActionListener() {	
			@Override
			public void actionPerformed(ActionEvent arg0) {			
				SwingUtilities.invokeLater(new Runnable() {
					public void run() {	
						frame.dispose();
						FunctionFrame functiondemo = new FunctionFrame();
						functiondemo.setVisible(true);
					}
				});		
			}
		});
	}
	
	private JComboBox jComboBox;	
	private JButton nextButton;
}
