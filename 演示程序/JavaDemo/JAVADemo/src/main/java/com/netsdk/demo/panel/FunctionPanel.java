package main.java.com.netsdk.demo.panel;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import main.java.com.netsdk.common.Res;
import main.java.com.netsdk.demo.DownLoadRecord;
import main.java.com.netsdk.demo.RealPlay;
import main.java.com.netsdk.demo.TrafficEvent;

/*
 * 切换语言面板
 */
public class FunctionPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	
	public FunctionPanel(final JFrame frame) {
		setLayout(new GridLayout(5, 1));
		
		setBorder(new EmptyBorder(30, 50, 0, 50));
		realplayBtn = new JButton(Res.string().getRealplay());
		itsEventBtn = new JButton(Res.string().getITSEvent());
		downloadBtn = new JButton(Res.string().getDownloadRecord());
		
		add(realplayBtn);
		add(itsEventBtn);
		add(downloadBtn);
		
		realplayBtn.addActionListener(new ActionListener() {		
			@Override
			public void actionPerformed(ActionEvent arg0) {		
				SwingUtilities.invokeLater(new Runnable() {
					public void run() {	
						frame.dispose();
						RealPlay.main(null);
					}
				});	
			}
		});
		
		downloadBtn.addActionListener(new ActionListener() {		
			@Override
			public void actionPerformed(ActionEvent arg0) {	
				SwingUtilities.invokeLater(new Runnable() {
					public void run() {		
						frame.dispose();
						DownLoadRecord.main(null);
					}
				});	
			}
		});
		
		
		itsEventBtn.addActionListener(new ActionListener() {		
			@Override
			public void actionPerformed(ActionEvent arg0) {		
				SwingUtilities.invokeLater(new Runnable() {
					public void run() {		
						frame.dispose();
						TrafficEvent.main(null);
					}
				});	
			}
		});
	}
	
	/*
	 * 功能列表组件
	 */
	private JButton realplayBtn;
	private JButton downloadBtn;
	private JButton itsEventBtn;
}


