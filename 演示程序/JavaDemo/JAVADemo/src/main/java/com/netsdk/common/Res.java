package main.java.com.netsdk.common;

import java.util.Locale;
import java.util.ResourceBundle;

import main.java.com.netsdk.lib.NetSDKLib;

public final class Res {
	
	private ResourceBundle bundle;
	
	private Res() {
		switchLanguage(LanguageType.Chinese);
	}
	
	private static class StringBundleHolder {
		private static Res instance = new Res();
	}
	
	public static Res string() {
		return StringBundleHolder.instance;
	}
	
	public static enum LanguageType {
		English,
		Chinese
	}
	
	public ResourceBundle getBundle() {
		return bundle;
	}
	
	/**
	 * \if ENGLISH_LANG
	 * Switch between Chinese and English
	 * \else
	 * 中英文切换
	 * \endif
	 */
	public void switchLanguage(LanguageType type) {
		switch(type) {
			case Chinese:
				bundle = ResourceBundle.getBundle("res", new Locale("zh", "CN"));
				break;
			case English:
				bundle = ResourceBundle.getBundle("res", new Locale("en", "US"));
				break;
			default:
				break;
		}
	}
	
	public String getSwitchLanguage() {
		return bundle.getString("SWITCH_LANGUAGE");
	}
	
	public String getRealplay() {
		return bundle.getString("REALPLAY");
	}
	
	public String getDownloadRecord() {
		return bundle.getString("DOWNLOAD_RECORD");
	}
	
	public String getITSEvent() {
		return bundle.getString("ITS_EVENT");
	}
	
	public String getOnline() {
		return bundle.getString("ONLINE");
	}
	
	public String getDisConnectReconnecting() {
		return bundle.getString("DISCONNECT_RECONNECTING");
	}
	
	public String getDisconnectLoginAgain() {
		return bundle.getString("DISCONNECT_LOGIN_AGAIN");
	}
	public String getPromptMessage() {
		return bundle.getString("PROMPT_MESSAGE");
	}
	
	public String getErrorMessage() {
		return bundle.getString("ERROR_MESSAGE");
	}
	
	public String getReconnectSucceed() {
		return bundle.getString("RECONNECT_SUCCEED");
	}
	
	public String getSucceed() {
		return bundle.getString("SUCCEED");
	}
	
	public String getFailed() {
		return bundle.getString("Failed");
	}
	
	public String getYear() {
		return bundle.getString("YEAR");
	}
	
	public String getMonth() {
		return bundle.getString("MONTH");
	}
	
	public String getDay() {
		return bundle.getString("DAY");
	}
	
	public String getHour() {
		return bundle.getString("HOUR");
	}
	
	public String getMinute() {
		return bundle.getString("MINUTE");
	}
	
	public String getSecond() {
		return bundle.getString("SECOND");
	}
	
	public String getSunday() {
		return bundle.getString("SUNDAY");
	}
	
	public String getMonday() {
		return bundle.getString("MONDAY");
	}
	
	public String getTuesday() {
		return bundle.getString("TUESDAY");
	}
	
	public String getWednesday() {
		return bundle.getString("WEDNESDAY");
	}
	
	public String getThursday() {
		return bundle.getString("THURSDAY");
	}
	
	public String getFriday() {
		return bundle.getString("FRIDAY");
	}
	
	public String getSaturday() {
		return bundle.getString("SATURDAY");
	}
	
	public String[] getWeek() {
		String[] weekdays = {getSunday(),
							 getMonday(),
				    		 getTuesday(),
				    		 getWednesday(),
				    		 getThursday(),
				    		 getFriday(),
				    		 getSaturday()
		};
		
		return weekdays;
	}
	
	public String getConfirm() {
		return bundle.getString("CONFIRM");
	}
	
	public String getCancel() {
		return bundle.getString("CANCEL");
	}
	
	public String getDateChooser() {
		return bundle.getString("DATE_CHOOSER");
	}
	
	public String getFunctionList() {
		return bundle.getString("FUNCTIONLIST");
	}
	
	/**
	 * 登录设备设备错误状态
	 */
	public String getErrorCode(int err) {
		String msg = "";
		switch(err) {
		case NetSDKLib.NET_INVALID_HANDLE:
			msg = bundle.getString("NET_INVALID_HANDLE"); 
			break;
		case NetSDKLib.NET_USER_FLASEPWD_TRYTIME: 
			msg = bundle.getString("NET_USER_FLASEPWD_TRYTIME"); 
			break;
		case NetSDKLib.NET_LOGIN_ERROR_PASSWORD:  
			msg = bundle.getString("NET_LOGIN_ERROR_PASSWORD"); 
			break;
		case NetSDKLib.NET_LOGIN_ERROR_USER: 
			msg = bundle.getString("NET_LOGIN_ERROR_USER"); 
			break;
		case NetSDKLib.NET_LOGIN_ERROR_TIMEOUT: 
			msg = bundle.getString("NET_LOGIN_ERROR_TIMEOUT"); 
			break;
		case NetSDKLib.NET_LOGIN_ERROR_RELOGGIN: 
			msg = bundle.getString("NET_LOGIN_ERROR_RELOGGIN"); 
			break;
		case NetSDKLib.NET_LOGIN_ERROR_LOCKED:
			msg = bundle.getString("NET_LOGIN_ERROR_LOCKED"); 
			break;
		case NetSDKLib.NET_LOGIN_ERROR_BLACKLIST:
			msg = bundle.getString("NET_LOGIN_ERROR_BLACKLIST"); 
			break;
		case NetSDKLib.NET_LOGIN_ERROR_BUSY:
			msg = bundle.getString("NET_LOGIN_ERROR_BUSY"); 
			break;
		case NetSDKLib.NET_LOGIN_ERROR_CONNECT:
			msg = bundle.getString("NET_LOGIN_ERROR_CONNECT"); 
			break;
		case NetSDKLib.NET_LOGIN_ERROR_NETWORK: 
			msg = bundle.getString("NET_LOGIN_ERROR_NETWORK"); 
			break;
		case NetSDKLib.NET_NO_RECORD_FOUND:
			msg = bundle.getString("NET_NO_RECORD_FOUND");
			break;
		case NetSDKLib.NET_OPEN_FILE_ERROR:
			msg = bundle.getString("NET_OPEN_FILE_ERROR");
			break;
		case NetSDKLib.NET_ERROR:
		  	msg = bundle.getString("NET_ERROR");
		    break;
		case NetSDKLib.NET_RETURN_DATA_ERROR:
			msg = bundle.getString("NET_RETURN_DATA_ERROR");
		    break;
		case NetSDKLib.NET_LOGIN_ERROR_USER_OR_PASSOWRD:
			msg = bundle.getString("NET_LOGIN_ERROR_USER_OR_PASSOWRD");
		    break;
	    default:
	    	msg = bundle.getString("NET_ERROR");
		    break;
		}
		
		return msg;
	}

	public String getLogin() {
		return bundle.getString("LOGIN");
	}
	
	public String getLogout() {
		return bundle.getString("LOGOUT");
	}
	
	public String getDeviceIp() {
		return bundle.getString("DEVICE_IP");
	}
	
	public String getPort() {
		return bundle.getString("DEVICE_PORT");
	}
	
	public String getUserName() {
		return bundle.getString("USERNAME");
	}
	
	public String getPassword() {
		return bundle.getString("PASSWORD");
	}
	
	
	public String getLoginFailed() {
		return bundle.getString("LOGIN_FAILED");
	}
	
	public String getInputDeviceIP() {
		return bundle.getString("PLEASE_INPUT_DEVICE_IP");
	}
	
	public String getInputDevicePort() {
		return bundle.getString("PLEASE_INPUT_DEVICE_PORT");
	}
	
	public String getInputUsername() {
		return bundle.getString("PLEASE_INPUT_DEVICE_USERNAME");
	}
	
	public String getInputPassword() {
		return bundle.getString("PLEASE_INPUT_DEVICE_PASSWORD");
	}
	
	public String getStartRealPlay() {
		return bundle.getString("START_REALPLAY");
	}
	
	public String getStopRealPlay() {
		return bundle.getString("STOP_REALPLAY");
	}
	
	public String getChannel() {
		return bundle.getString("CHANNEL");
	}
	
	public String getStreamType() {
		return bundle.getString("STREAM_TYPE");
	}
	
	public String getMasterAndSub() {
		return bundle.getString("MASTER_AND_SUB_STREAM");
	}
	
	public String getMasterStream() {
		return bundle.getString("MASTER_STREAM");
	}
	
	public String getSubStream() {
		return bundle.getString("SUB_STREAM");
	}
	
	public String getSnap() {
		return bundle.getString("SNAP");
	}
	
	public String getSnapPicture() {
		return bundle.getString("SNAP_PICTURE");
	}
	
	public String getPTZControl() {
		return bundle.getString("PTZ_CONTROL");
	}
	
	public String getLeftUp() {
		return bundle.getString("LEFT_UP");
	}
	
	public String getUp() {
		return bundle.getString("UP");
	}
	
	public String getRightUp() {
		return bundle.getString("RIGHT_UP");
	}
	
	public String getLeft() {
		return bundle.getString("LEFT");
	}
	
	public String getRight() {
		return bundle.getString("RIGHT");
	}
	
	public String getLeftDown() {
		return bundle.getString("LEFT_DOWN");
	}
	
	public String getDown() {
		return bundle.getString("DOWN");
	}
	
	public String getRightDown() {
		return bundle.getString("RIGHT_DOWN");
	}
	
	public String getSpeed() {
		return bundle.getString("SPEED");
	}
	
	public String getZoomAdd() {
		return bundle.getString("ZOOM_ADD");
	}
	
	public String getZoomDec() {
		return bundle.getString("ZOOM_DEC");
	}
	
	public String getFocusAdd() {
		return bundle.getString("FOCUS_ADD");
	}
	
	public String getFocusDec() {
		return bundle.getString("FOCUS_DEC");
	}
	
	public String getIrisAdd() {
		return bundle.getString("IRIS_ADD");
	}
	
	public String getIrisDec() {
		return bundle.getString("IRIS_DEC");
	}
	
	public String getIndex() {
		return bundle.getString("INDEX");
	}
	
	public String getEventPicture() {
		return bundle.getString("EVENT_PICTURE");
	}
	
	public String getPlatePicture() {
		return bundle.getString("PLATE_PICTURE");
	}
	
	public String getEventName() {
		return bundle.getString("EVENT_NAME");
	}
	
	public String getLicensePlate() {
		return bundle.getString("LICENSE_PLATE");
	}
	
	public String getEventTime() {
		return bundle.getString("EVENT_TIME");
	}
	
	public String getPlateType() {
		return bundle.getString("PLATE_TYPE");
	}
	
	public String getPlateColor() {
		return bundle.getString("PLATE_COLOR");
	}
	
	public String getVehicleColor() {
		return bundle.getString("VEHICLE_COLOR");
	}
	
	public String getVehicleType() {
		return bundle.getString("VEHICLE_TYPE");
	}
	
	public String getVehicleSize() {
		return bundle.getString("VEHICLE_SIZE");
	}
	
	public String getFileCount() {
		return bundle.getString("FILE_COUNT");
	}
	
	public String getFileIndex() {
		return bundle.getString("FILE_INDEX");
	}
	
	public String getGroupId() {
		return bundle.getString("GROUP_ID");
	}
	
	public String getIllegalPlace() {
		return bundle.getString("ILLEGAL_PLACE");
	}
	
	public String getLaneNumber() {
		return bundle.getString("LANE_NUMBER");
	}
	
	public String getEventInfo() {
		return bundle.getString("EVENT_INFO");
	}
	
	public String getNoPlate() {
		return bundle.getString("NO_PLATENUMBER");
	}
	
	public String[] getTrafficTableName() {
		String[] name = {getIndex(),
						 getEventName(), 
						 getLicensePlate(), 
						 getEventTime(), 
						 getPlateType(), 	
						 getPlateColor(), 	
						 getVehicleColor(), 
						 getVehicleType(), 
						 getVehicleSize(),
						 getFileCount(),
						 getFileIndex(),
						 getGroupId(),
						 getIllegalPlace(),
						 getLaneNumber()};
		return name;
	}
	
	public String getOperate() {
		return bundle.getString("OPERATE");
	}
	
	public String getAttach() {
		return bundle.getString("ATTACH");
	}
	
	public String getDetach() {
		return bundle.getString("DETACH");
	}
	
	public String getOpenStrobe() {
		return bundle.getString("OPEN_STROBE");
	}
	
	public String getCloseStrobe() {
		return bundle.getString("CLOSE_STROBE");
	}
	
	public String getOpenStrobeFailed() {
		return bundle.getString("OPEN_STROBE_FAILED");
	}
	
	public String getManualCapture() {
		return bundle.getString("MANUAL_CAPTURE");
	}
	
	public String getManualCaptureSucceed() {
		return  bundle.getString("MANUALSNAP_SUCCEED");
	}
	
	public String getManualCaptureFailed() {
		return  bundle.getString("MANUALSNAP_FAILED");
	}
    
    /*
     * 车辆大小对照表
     */
    public String getTrafficSize(int nVehicleSize) {
    	String vehicleClass = "";
    	for(int i = 0; i < 5; i++) {
      		if( ((byte)nVehicleSize & (1 << i)) > 0 ) {
      			switch (i) {
				case 0:
					vehicleClass = bundle.getString("LIGHT_DUTY");
					break;
				case 1:
					vehicleClass = bundle.getString("MEDIUM");
					break;
				case 2:
					vehicleClass = bundle.getString("OVER_SIZE");
					break;
				case 3:
					vehicleClass = bundle.getString("MINI_SIZE");
					break;
				case 4:
					vehicleClass = bundle.getString("LARGE_SIZE");
					break;
				default:
					break;
				}
      		}
      	} 
    	
    	return vehicleClass;
    }
	
    /*
     * 获取事件名称
     */
    public String getEventName(int type) {
    	String name = "";
    	switch (type) {
			case NetSDKLib.EVENT_IVS_TRAFFICJUNCTION:  ///< 交通路口事件
				name = bundle.getString("EVENT_IVS_TRAFFICJUNCTION");
				break;
			case NetSDKLib.EVENT_IVS_TRAFFIC_RUNREDLIGHT: ///< 闯红灯事件
				name = bundle.getString("EVENT_IVS_TRAFFIC_RUNREDLIGHT");
				break;
			case NetSDKLib.EVENT_IVS_TRAFFIC_OVERLINE: ///< 压车道线事件
				name = bundle.getString("EVENT_IVS_TRAFFIC_OVERLINE");
				break;
			case NetSDKLib.EVENT_IVS_TRAFFIC_RETROGRADE: ///< 逆行事件
				name = bundle.getString("EVENT_IVS_TRAFFIC_RETROGRADE");
				break;
			case NetSDKLib.EVENT_IVS_TRAFFIC_TURNLEFT: ///< 违章左转
				name = bundle.getString("EVENT_IVS_TRAFFIC_TURNLEFT");
				break;
			case NetSDKLib.EVENT_IVS_TRAFFIC_TURNRIGHT: ///< 违章右转
				name = bundle.getString("EVENT_IVS_TRAFFIC_TURNRIGHT");
				break;
			case NetSDKLib.EVENT_IVS_TRAFFIC_UTURN: ///< 违章掉头
				name = bundle.getString("EVENT_IVS_TRAFFIC_UTURN");
				break;
			case NetSDKLib.EVENT_IVS_TRAFFIC_OVERSPEED: ///< 超速
				name = bundle.getString("EVENT_IVS_TRAFFIC_OVERSPEED");
				break;
			case NetSDKLib.EVENT_IVS_TRAFFIC_UNDERSPEED: ///< 低速
				name = bundle.getString("EVENT_IVS_TRAFFIC_UNDERSPEED");
				break;
			case NetSDKLib.EVENT_IVS_TRAFFIC_PARKING: ///< 违章停车
				name = bundle.getString("EVENT_IVS_TRAFFIC_PARKING");
				break;
			case NetSDKLib.EVENT_IVS_TRAFFIC_WRONGROUTE: ///< 不按车道行驶
				name = bundle.getString("EVENT_IVS_TRAFFIC_WRONGROUTE");
				break;
			case NetSDKLib.EVENT_IVS_TRAFFIC_CROSSLANE: ///< 违章变道
				name = bundle.getString("EVENT_IVS_TRAFFIC_CROSSLANE");
				break;
			case NetSDKLib.EVENT_IVS_TRAFFIC_OVERYELLOWLINE: ///< 压黄线
				name = bundle.getString("EVENT_IVS_TRAFFIC_OVERYELLOWLINE");
				break;
			case NetSDKLib.EVENT_IVS_TRAFFIC_YELLOWPLATEINLANE: ///< 黄牌车占道事件
				name = bundle.getString("EVENT_IVS_TRAFFIC_YELLOWPLATEINLANE");
				break;
			case NetSDKLib.EVENT_IVS_TRAFFIC_PEDESTRAINPRIORITY: ///< 斑马线行人优先事件
				name = bundle.getString("EVENT_IVS_TRAFFIC_PEDESTRAINPRIORITY");
				break;
			case NetSDKLib.EVENT_IVS_TRAFFIC_MANUALSNAP: ///< 交通手动抓拍事件
				name = bundle.getString("EVENT_IVS_TRAFFIC_MANUALSNAP");
				break;
			case NetSDKLib.EVENT_IVS_TRAFFIC_VEHICLEINROUTE: ///< 有车占道事件
				name = bundle.getString("EVENT_IVS_TRAFFIC_VEHICLEINROUTE");
				break;
			case NetSDKLib.EVENT_IVS_TRAFFIC_VEHICLEINBUSROUTE: ///< 占用公交车道事件
				name = bundle.getString("EVENT_IVS_TRAFFIC_VEHICLEINBUSROUTE");
				break;
			case NetSDKLib.EVENT_IVS_TRAFFIC_BACKING: ///< 违章倒车事件
				name = bundle.getString("EVENT_IVS_TRAFFIC_BACKING");
				break;
			case NetSDKLib.EVENT_IVS_TRAFFIC_PARKINGSPACEPARKING: ///< 车位有车事件
				name = bundle.getString("EVENT_IVS_TRAFFIC_PARKINGSPACEPARKING");
				break;
			case NetSDKLib.EVENT_IVS_TRAFFIC_PARKINGSPACENOPARKING: ///< 车位无车事件
				name = bundle.getString("EVENT_IVS_TRAFFIC_PARKINGSPACENOPARKING");
				break;
			case NetSDKLib.EVENT_IVS_TRAFFIC_WITHOUT_SAFEBELT: ///< 交通未系安全带事件
				name = bundle.getString("EVENT_IVS_TRAFFIC_WITHOUT_SAFEBELT");
				break;
			default:
				break;
		}
    	
    	return name;
    }
	
    public String getRecordType() {
    	return bundle.getString("RECORD_TYPE");
    }
    
    public String getStartTime() {
    	return bundle.getString("START_TIME");
    }
    
    public String getEndTime() {
    	return bundle.getString("END_TIME");
    }
    
    public String[] getDownloadTableName() {
    	String[] name = {getIndex(), 
						 getChannel(),
						 getRecordType(), 
						 getStartTime(), 
						 getEndTime()}; 	
    	return name;
    }
    
    public String getDownloadByFile() {
    	return bundle.getString("DOWNLOAD_RECORD_BYFILE");
    }
    
    public String getQuery() {
    	return bundle.getString("QUERY");
    }
    
    public String getDownload() {
    	return bundle.getString("DOWNLOAD");
    }
    
    public String getStopDownload() {
    	return bundle.getString("STOP_DOWNLOAD");
    }
    
    public String getDownloadByTime() {
    	return bundle.getString("DOWNLOAD_RECORD_BYTIME");
    }
    
    public String getSelectTimeAgain() {
    	return bundle.getString("PLEASE_SELECT_TIME_AGAIN");
    }
    
    public String getSelectRowWithData() {
    	return bundle.getString("PLEASE_FIRST_SELECT_ROW_WITH_DATA");
    }
    
    public String getQueryRecord() {
    	return bundle.getString("PLEASE_FIRST_QUERY_RECORD");
    }
    
    public String getDownloadCompleted() {
    	return bundle.getString("DOWNLOAD_COMPLETED");
    }
    
	/**
	 * 获取录像类型
	 */
	public String getRecordTypeStr(int nRecordFileType) {
		String recordTypeStr = "";
		switch(nRecordFileType) {
			case 0:
                recordTypeStr = bundle.getString("GENERAL_RECORD");
                break;
			case 1:
			    recordTypeStr = bundle.getString("ALARM_RECORD");
                break;
			case 2:
			    recordTypeStr = bundle.getString("MOTION_DETECTION");
                break;
			case 3:
			    recordTypeStr = bundle.getString("CARD_NUMBER_RECORD");
                break;
            default:
            	break;
		}
		
		return recordTypeStr;
	}
    
	public int getRecordTypeInt(String recordFileStr) {
		int recordType = -1;
		if(recordFileStr.equals(bundle.getString("GENERAL_RECORD"))) {
			recordType = 0;
		} else if(recordFileStr.equals(bundle.getString("ALARM_RECORD"))) {
			recordType = 1;
		} else if(recordFileStr.equals(bundle.getString("MOTION_DETECTION"))) {
			recordType = 2;
		} else if(recordFileStr.equals(bundle.getString("CARD_NUMBER_RECORD"))) {
			recordType = 3;
		} 
		
		return recordType;
	}  
    
    
    
    
}
