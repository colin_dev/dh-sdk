package main.java.com.netsdk.demo.frame;

import java.awt.BorderLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

import main.java.com.netsdk.demo.panel.PaintPanel;

/*
 * 智能交通列表双击展示图片框架
 */
public class ListPictureShowFrame extends JFrame {
	private static final long serialVersionUID = 1L;
	public ListPictureShowFrame() {
	    setSize(800, 600);
	    setLayout(new BorderLayout());
	    setLocationRelativeTo(null);
	    
	    listPanel = new PaintPanel();
	    add(listPanel, BorderLayout.CENTER);
	    
	    addWindowListener(new WindowAdapter() {
	    	public void windowClosing(WindowEvent e) {
	    		dispose();		
	    	}
	    });
	}
	
	public PaintPanel listPanel;
}