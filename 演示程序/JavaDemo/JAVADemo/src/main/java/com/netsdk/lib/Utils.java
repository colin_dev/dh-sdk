package main.java.com.netsdk.lib;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import com.sun.jna.Platform;

public class Utils {
	public Utils() {

	}
	
	// 获取操作平台信息
	public static String getOsPrefix() {
		String arch = System.getProperty("os.arch").toLowerCase();
		final String name = System.getProperty("os.name");
		String osPrefix;
		switch(Platform.getOSType()) {
			case Platform.WINDOWS: {
				if ("i386".equals(arch))
	                arch = "x86";
	            osPrefix = "win32-" + arch;
			}
            break;
			case Platform.LINUX: {
				if ("x86".equals(arch)) {
	                arch = "i386";
	            }
	            else if ("x86_64".equals(arch)) {
	                arch = "amd64";
	            }
	            osPrefix = "linux-" + arch;
			}			       
	        break;
			default: {
	            osPrefix = name.toLowerCase();
	            if ("x86".equals(arch)) {
	                arch = "i386";
	            }
	            if ("x86_64".equals(arch)) {
	                arch = "amd64";
	            }
	            int space = osPrefix.indexOf(" ");
	            if (space != -1) {
	                osPrefix = osPrefix.substring(0, space);
	            }
	            osPrefix += "-" + arch;
			}
	        break;
	       
		}

		return osPrefix;
	}	

	// 读取当前目录下的路径配置文件
	public static String readConfigProperties(String key) {
		Properties properties = new Properties();
		String rootPath = System.getProperty("user.dir").replace("\\", "/");  // 获取当前目录
		FileInputStream inputStream = null;
		try {
			inputStream = new FileInputStream(rootPath + "/config.properties");
			properties.load(inputStream);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}catch (IOException e) {
			e.printStackTrace();
		}
		
		return properties.getProperty(key);
	}
	
    public static String getOsName() {
    	String osName = "";
		String osPrefix = getOsPrefix();
		if(osPrefix.toLowerCase().startsWith("win32-x86")
				||osPrefix.toLowerCase().startsWith("win32-amd64") ) {
			osName = "win";
		} else if(osPrefix.toLowerCase().startsWith("linux-i386")
				|| osPrefix.toLowerCase().startsWith("linux-amd64")) {
			osName = "linux";
		}
		
		return osName;
    }
    
    // 获取加载库
	public static String getLoadLibrary(String library) {
		String loadLibrary = "";
		String osPrefix = getOsPrefix();
		if(osPrefix.toLowerCase().startsWith("win32-x86")) {
			loadLibrary = readConfigProperties("win32-x86");
		} else if(osPrefix.toLowerCase().startsWith("win32-amd64") ) {
			loadLibrary = readConfigProperties("win32-amd64");
		} else if(osPrefix.toLowerCase().startsWith("linu-i386")) {
			loadLibrary = readConfigProperties("linu-i386");
		}else if(osPrefix.toLowerCase().startsWith("linux-amd64")) {
			loadLibrary = readConfigProperties("linux-amd64");
		}

		return loadLibrary + library;
	}
}
