package main.java.com.netsdk.lib;

import java.awt.Panel;
import java.io.File;

import main.java.com.netsdk.common.Res;
import main.java.com.netsdk.lib.ToolKits;
import main.java.com.netsdk.lib.NetSDKLib;
import com.sun.jna.Native;
import com.sun.jna.NativeLong;
import com.sun.jna.ptr.IntByReference;

public class INetSDK {
	private static NetSDKLib netsdk = NetSDKLib.NETSDK_INSTANCE;
	
	// 设备信息
	public static NetSDKLib.NET_DEVICEINFO_Ex m_stDeviceInfo = new NetSDKLib.NET_DEVICEINFO_Ex();
	
	// 登陆句柄
	public static NativeLong m_hLoginHandle = new NativeLong(0);   
	
	// 预览句柄
	public static NativeLong m_hPlayHandle = new NativeLong(0);
	
	// 智能订阅句柄
	public static NativeLong m_hAttachHandle = new NativeLong(0);
	
	// 下载句柄
	public static NativeLong m_hDownLoadHandle = new NativeLong(0);
		
	private static boolean bInit    = false;
	private static boolean bLogopen = false;
	
	/**
	 * \if ENGLISH_LANG
	 * Init
	 * \else
	 * 初始化
	 * \endif
	 */
	public static boolean init(NetSDKLib.fDisConnect disConnect, NetSDKLib.fHaveReConnect haveReConnect) {	
		bInit = netsdk.CLIENT_Init(disConnect, new NativeLong(0));
		if(!bInit) {
			System.out.println("Initialize SDK failed");
			return false;
		}

		//打开日志，可选
		NetSDKLib.LOG_SET_PRINT_INFO setLog = new NetSDKLib.LOG_SET_PRINT_INFO();
        File path = new File("./sdklog/");
        if (!path.exists()) {
            path.mkdir();
        }
		String logPath = path.getAbsoluteFile().getParent() + "\\sdklog\\" + ToolKits.getDate() + ".log";
		setLog.nPrintStrategy = 0;
		setLog.bSetFilePath = 1;
		System.arraycopy(logPath.getBytes(), 0, setLog.szLogFilePath, 0, logPath.getBytes().length);
		System.out.println(logPath);
		setLog.bSetPrintStrategy = 1;
		setLog.nPrintStrategy    = 0;
		bLogopen = netsdk.CLIENT_LogOpen(setLog);
		if(!bLogopen ) {
			System.err.println("Failed to open NetSDK log");
		}
		
		// 设置断线重连回调接口，设置过断线重连成功回调函数后，当设备出现断线情况，SDK内部会自动进行重连操作
		// 此操作为可选操作，但建议用户进行设置
		netsdk.CLIENT_SetAutoReconnect(haveReConnect, new NativeLong(0));
	    
		//设置登录超时时间和尝试次数，可选
		int waitTime = 5000; //登录请求响应超时时间设置为5S
		int tryTimes = 1;    //登录时尝试建立链接1次
		netsdk.CLIENT_SetConnectTime(waitTime, tryTimes);
		
		// 设置更多网络参数，NET_PARAM的nWaittime，nConnectTryNum成员与CLIENT_SetConnectTime 
		// 接口设置的登录设备超时时间和尝试次数意义相同,可选
		NetSDKLib.NET_PARAM netParam = new NetSDKLib.NET_PARAM();
		netParam.nConnectTime = 10000; //登录时尝试建立链接的超时时间
		netsdk.CLIENT_SetNetworkParam(netParam);	
		
		return true;
	}
	
	/**
	 * \if ENGLISH_LANG
	 * CleanUp
	 * \else
	 * 清除环境
	 * \endif
	 */
	public static void cleanup() {
		if(bLogopen) {
			netsdk.CLIENT_LogClose();
		}
		
		if(bInit) {
			netsdk.CLIENT_Cleanup();
		}
	}

	/**
	 * \if ENGLISH_LANG
	 * Login Device
	 * \else
	 * 登录设备
	 * \endif
	 */
	public static boolean login(String m_strIp, int m_nPort, String m_strUser, String m_strPassword) {	
		IntByReference nError = new IntByReference(0);
		m_hLoginHandle = netsdk.CLIENT_LoginEx2(m_strIp, m_nPort, m_strUser, m_strPassword, 0, null, m_stDeviceInfo, nError);
		if(m_hLoginHandle.longValue() == 0) {
			System.err.printf("Login Device[%s] Port[%d]Failed. Last Error[0x%x]\n", m_strIp, m_nPort, netsdk.CLIENT_GetLastError());
		} else {
			System.out.println("Login Success [ " + m_strIp + " ]");
		}	
		
		return m_hLoginHandle.longValue() == 0? false:true;
	}
	
	/**
	 * \if ENGLISH_LANG
	 * Logout Device
	 * \else
	 * 登出设备
	 * \endif
	 */
	public static boolean logout() {
		if(m_hLoginHandle.longValue() == 0) {
			return false;
		}
		
		boolean bRet = netsdk.CLIENT_Logout(m_hLoginHandle);
		if(bRet) {			
			m_hLoginHandle.setValue(0);	
		}
		
		return bRet;
	}
	
	/**
	 * 登录设备设备错误状态
	 */
	public static String getErrorCode() {
		return Res.string().getErrorCode(netsdk.CLIENT_GetLastError());
	}
	
	/**
	 * \if ENGLISH_LANG
	 * Start RealPlay
	 * \else
	 * 开始预览
	 * \endif
	 */
	public static boolean startRealPlay(int channel, int stream, Panel realPlayWindow) {
	    m_hPlayHandle = netsdk.CLIENT_RealPlayEx(m_hLoginHandle, channel, Native.getComponentPointer(realPlayWindow), stream);
	
	    if(m_hPlayHandle.longValue() == 0) {
	  	    System.err.println("开始实时监视失败，错误码" + String.format("[0x%x]", netsdk.CLIENT_GetLastError()));
	    } else {
	  	    System.out.println("Success to start realplay"); 
	    }
	    
	    return m_hPlayHandle.longValue() == 0? false:true;
	} 
	
	/**
	 * \if ENGLISH_LANG
	 * Start RealPlay
	 * \else
	 * 停止预览
	 * \endif
	 */
	public static void stopRealPlay() {
		if(m_hPlayHandle.longValue() == 0) {
			return;
		}
		
		boolean bRet = netsdk.CLIENT_StopRealPlayEx(m_hPlayHandle);
		if(bRet) {
			m_hPlayHandle.setValue(0);
		}
	}
	
	public static void SetSnapRevCallBack(NetSDKLib.fSnapRev m_SnapReceiveCB){ 
		//设置抓图回调函数， 图片主要在m_SnapReceiveCB中返回
		netsdk.CLIENT_SetSnapRevCallBack(m_SnapReceiveCB, new NativeLong(0));
	}
	
	/**
	 * 远程抓图
	 */
	public static boolean snapPicture(int chn) {
		// 发送抓图命令给前端设备，抓图的信息
		NetSDKLib.SNAP_PARAMS stuSnapParams = new NetSDKLib.SNAP_PARAMS() ; 
		stuSnapParams.Channel = chn;  //抓图通道
		stuSnapParams.mode = 0;     //表示请求一帧 
		stuSnapParams.Quality = 3;
		stuSnapParams.InterSnap = 5;
		stuSnapParams.CmdSerial = 0; // 请求序列号，有效值范围 0~65535，超过范围会被截断为  
		
		IntByReference reserved = new IntByReference(0);
		if (!netsdk.CLIENT_SnapPictureEx(m_hLoginHandle, stuSnapParams, reserved)) { 
			System.err.printf("CLIENT_SnapPictureEx Failed!Last Error[%x]\n", netsdk.CLIENT_GetLastError());
			return false;
		} else { 
			System.out.println("CLIENT_SnapPictureEx success"); 
		}
		return true;
	}
	
	/********************************************************************************
	 * 									云台功能                  							*
	 ********************************************************************************/
	/**
	 * 向上
	 */
	public static boolean ptzControlUpStart(int nChannelID, int lParam1, int lParam2) {
		return netsdk.CLIENT_DHPTZControlEx(m_hLoginHandle, nChannelID, 
									NetSDKLib.NET_PTZ_ControlType.NET_PTZ_UP_CONTROL, 
									lParam1, lParam2, 0, 0);
	}
	public static boolean ptzControlUpEnd(int nChannelID) {
		return netsdk.CLIENT_DHPTZControlEx(m_hLoginHandle, nChannelID, 
									 NetSDKLib.NET_PTZ_ControlType.NET_PTZ_UP_CONTROL, 
									 0, 0, 0, 1);
	}
	
	/**
	 * 向下
	 */
	public static boolean ptzControlDownStart(int nChannelID, int lParam1, int lParam2) {
		return netsdk.CLIENT_DHPTZControlEx(m_hLoginHandle, nChannelID, 
											NetSDKLib.NET_PTZ_ControlType.NET_PTZ_DOWN_CONTROL, 
											lParam1, lParam2, 0, 0);
	}
	public static boolean ptzControlDownEnd(int nChannelID) {
		return 	netsdk.CLIENT_DHPTZControlEx(m_hLoginHandle, nChannelID, 
											 NetSDKLib.NET_PTZ_ControlType.NET_PTZ_DOWN_CONTROL, 
											 0, 0, 0, 1);
	}
	
	/**
	 * 向左
	 */
	public static boolean ptzControlLeftStart(int nChannelID, int lParam1, int lParam2) {
		return netsdk.CLIENT_DHPTZControlEx(m_hLoginHandle, nChannelID, 
											NetSDKLib.NET_PTZ_ControlType.NET_PTZ_LEFT_CONTROL, 
											lParam1, lParam2, 0, 0);	
	}
	public static boolean ptzControlLeftEnd(int nChannelID) {
		return	netsdk.CLIENT_DHPTZControlEx(m_hLoginHandle, nChannelID, 
											 NetSDKLib.NET_PTZ_ControlType.NET_PTZ_LEFT_CONTROL, 
											 0, 0, 0, 1);	
	}
	
	/**
	 * 向右
	 */
	public static boolean ptzControlRightStart(int nChannelID, int lParam1,int lParam2) {
		return netsdk.CLIENT_DHPTZControlEx(m_hLoginHandle, nChannelID, 
											NetSDKLib.NET_PTZ_ControlType.NET_PTZ_RIGHT_CONTROL, 
											lParam1, lParam2, 0, 0);
	}
	public static boolean ptzControlRightEnd(int nChannelID) {
		return netsdk.CLIENT_DHPTZControlEx(m_hLoginHandle, nChannelID, 
											 NetSDKLib.NET_PTZ_ControlType.NET_PTZ_RIGHT_CONTROL, 
											 0, 0, 0, 1);		
	}
	
	/**
	 * 向左上
	 */
	public static boolean ptzControlLeftUpStart(int nChannelID, int lParam1, int lParam2) {
		return netsdk.CLIENT_DHPTZControlEx(m_hLoginHandle, nChannelID, 
											NetSDKLib.NET_EXTPTZ_ControlType.NET_EXTPTZ_LEFTTOP, 
											lParam1, lParam2, 0, 0);
	}
	public static boolean ptzControlLeftUpEnd(int nChannelID) {
		return netsdk.CLIENT_DHPTZControlEx(m_hLoginHandle, nChannelID, 
											 NetSDKLib.NET_EXTPTZ_ControlType.NET_EXTPTZ_LEFTTOP, 
											 0, 0, 0, 1);	
	}
	
	/**
	 * 向右上
	 */
	public static boolean ptzControlRightUpStart(int nChannelID, int lParam1, int lParam2) {
		return netsdk.CLIENT_DHPTZControlEx(m_hLoginHandle, nChannelID, 
											NetSDKLib.NET_EXTPTZ_ControlType.NET_EXTPTZ_RIGHTTOP, 
											lParam1, lParam2, 0, 0);
	}
	public static boolean ptzControlRightUpEnd(int nChannelID) {
		return netsdk.CLIENT_DHPTZControlEx(m_hLoginHandle, nChannelID, 
											 NetSDKLib.NET_EXTPTZ_ControlType.NET_EXTPTZ_RIGHTTOP, 
											 0, 0, 0, 1);	
	}

	/**
	 * 向左下
	 */
	public static boolean ptzControlLeftDownStart(int nChannelID, int lParam1, int lParam2) {
		return netsdk.CLIENT_DHPTZControlEx(m_hLoginHandle, nChannelID, 
													NetSDKLib.NET_EXTPTZ_ControlType.NET_EXTPTZ_LEFTDOWN, 
													lParam1, lParam2, 0, 0);
	}
	public static boolean ptzControlLeftDownEnd(int nChannelID) {
		return netsdk.CLIENT_DHPTZControlEx(m_hLoginHandle, nChannelID, 
										 NetSDKLib.NET_EXTPTZ_ControlType.NET_EXTPTZ_LEFTDOWN, 
										 0, 0, 0, 1);
	}
	
	/**
	 * 向右下
	 */
	public static boolean ptzControlRightDownStart(int nChannelID, int lParam1, int lParam2) {
		return netsdk.CLIENT_DHPTZControlEx(m_hLoginHandle, nChannelID, 
													NetSDKLib.NET_EXTPTZ_ControlType.NET_EXTPTZ_RIGHTDOWN, 
													lParam1, lParam2, 0, 0);
	}
	public static boolean ptzControlRightDownEnd(int nChannelID) {
		return netsdk.CLIENT_DHPTZControlEx(m_hLoginHandle, nChannelID, 
										 NetSDKLib.NET_EXTPTZ_ControlType.NET_EXTPTZ_RIGHTDOWN, 
										 0, 0, 0, 1);
	}
	
    /**
     * 变倍+
     */
    public static boolean ptzControlZoomAddStart(int nChannelID, int lParam2) {
        return netsdk.CLIENT_DHPTZControlEx(m_hLoginHandle, nChannelID, 
							        	   NetSDKLib.NET_PTZ_ControlType.NET_PTZ_ZOOM_ADD_CONTROL, 
							        	   0, lParam2, 0, 0);
    }
    public static boolean ptzControlZoomAddEnd(int nChannelID) {
        return netsdk.CLIENT_DHPTZControlEx(m_hLoginHandle, nChannelID, 
					            		    NetSDKLib.NET_PTZ_ControlType.NET_PTZ_ZOOM_ADD_CONTROL, 
					            		    0, 0, 0, 1);
    }

    /**
     * 变倍-
     */
    public static boolean ptzControlZoomDecStart(int nChannelID, int lParam2) {
       return netsdk.CLIENT_DHPTZControlEx(m_hLoginHandle, nChannelID, 
							        	    NetSDKLib.NET_PTZ_ControlType.NET_PTZ_ZOOM_DEC_CONTROL, 
							        	    0, lParam2, 0, 0);
    }
    public static boolean ptzControlZoomDecEnd(int nChannelID) {
        return netsdk.CLIENT_DHPTZControlEx(m_hLoginHandle, nChannelID, 
					            		     NetSDKLib.NET_PTZ_ControlType.NET_PTZ_ZOOM_DEC_CONTROL, 
					            		     0, 0, 0, 1);
    }

    /**
     * 变焦+
     */
    public static boolean ptzControlFocusAddStart(int nChannelID, int lParam2) {
    	return netsdk.CLIENT_DHPTZControlEx(m_hLoginHandle, nChannelID, 
									        	    NetSDKLib.NET_PTZ_ControlType.NET_PTZ_FOCUS_ADD_CONTROL, 
									        	    0, lParam2, 0, 0);
    }
    public static boolean ptzControlFocusAddEnd(int nChannelID) {
    	return netsdk.CLIENT_DHPTZControlEx(m_hLoginHandle, nChannelID, 
				            		     NetSDKLib.NET_PTZ_ControlType.NET_PTZ_FOCUS_ADD_CONTROL, 
				            		     0, 0, 0, 1);
    }

    /**
     * 变焦-
     */
    public static boolean ptzControlFocusDecStart(int nChannelID, int lParam2) {
        return netsdk.CLIENT_DHPTZControlEx(m_hLoginHandle, nChannelID, 
							        	    NetSDKLib.NET_PTZ_ControlType.NET_PTZ_FOCUS_DEC_CONTROL, 
							        	    0, lParam2, 0, 0);
    }
    public static boolean ptzControlFocusDecEnd(int nChannelID) {
        return netsdk.CLIENT_DHPTZControlEx(m_hLoginHandle, nChannelID, 
					            		     NetSDKLib.NET_PTZ_ControlType.NET_PTZ_FOCUS_DEC_CONTROL, 
					            		     0, 0, 0, 1);
    }

    /**
     * 光圈+
     */
    public static boolean ptzControlIrisAddStart(int nChannelID, int lParam2) {
        return netsdk.CLIENT_DHPTZControlEx(m_hLoginHandle, nChannelID, 
							        	    NetSDKLib.NET_PTZ_ControlType.NET_PTZ_APERTURE_ADD_CONTROL, 
							        	    0, lParam2, 0, 0);
    }
    public static boolean ptzControlIrisAddEnd(int nChannelID) {
        return netsdk.CLIENT_DHPTZControlEx(m_hLoginHandle, nChannelID, 
					            		     NetSDKLib.NET_PTZ_ControlType.NET_PTZ_APERTURE_ADD_CONTROL, 
					            		     0, 0, 0, 1);
    }

    /**
     * 光圈-
     */
    public static boolean ptzControlIrisDecStart(int nChannelID, int lParam2) {
        return netsdk.CLIENT_DHPTZControlEx(m_hLoginHandle, nChannelID, 
									        	    NetSDKLib.NET_PTZ_ControlType.NET_PTZ_APERTURE_DEC_CONTROL, 
									        	    0, lParam2, 0, 0);
    }
    public static boolean ptzControlIrisDecEnd(int nChannelID) {
        return netsdk.CLIENT_DHPTZControlEx(m_hLoginHandle, nChannelID, 
					            		     NetSDKLib.NET_PTZ_ControlType.NET_PTZ_APERTURE_DEC_CONTROL, 
					            		     0, 0, 0, 1);
    }
	
	/********************************************************************************
	 * 									智能交通功能                  						*
	 ********************************************************************************/
	/**
	 * 老版本开闸按钮
	 */
	public static void Old_OpenStrobe(int bTriggerBtnClick) {
		if (m_hLoginHandle.longValue() != 0) {
			System.out.println("Trigger Button Action");
			
			NetSDKLib.ALARMCTRL_PARAM param = new NetSDKLib.ALARMCTRL_PARAM();
			param.nAction = bTriggerBtnClick;; // 1：触发报警；0：停止报警. 按钮按下
			if (netsdk.CLIENT_ControlDeviceEx(m_hLoginHandle, NetSDKLib.CtrlType.CTRLTYPE_TRIGGER_ALARM_OUT, param.getPointer(), null, 3000)) {
				System.out.println("控制成功");
			}else {
				System.err.printf("Failed to Open 0x%x", netsdk.CLIENT_GetLastError());
			}
		}
	}
	
	/**
	 * 新版本开闸
	 */
	public static boolean New_OpenStrobe() {		
		NetSDKLib.NET_CTRL_OPEN_STROBE openStrobe = new NetSDKLib.NET_CTRL_OPEN_STROBE();
		openStrobe.nChannelId = 0;
		String plate = new String("浙A888888");
		
		System.arraycopy(plate.getBytes(), 0, openStrobe.szPlateNumber, 0, plate.getBytes().length);
		openStrobe.write();
		if (netsdk.CLIENT_ControlDeviceEx(m_hLoginHandle, NetSDKLib.CtrlType.CTRLTYPE_CTRL_OPEN_STROBE, openStrobe.getPointer(), null, 3000)) {
		    System.out.println("Open Success!");
		} else {
			System.err.printf("Failed to Open 0x%x\n", netsdk.CLIENT_GetLastError());
			return false;
		} 
		openStrobe.read();
		
		return true;
	}
	
	/**
	 * 新版本关闸
	 */
	public static void New_CloseStrobe() {	
		NetSDKLib.NET_CTRL_CLOSE_STROBE closeStrobe = new NetSDKLib.NET_CTRL_CLOSE_STROBE();
        closeStrobe.nChannelId = 0;
        closeStrobe.write();
        if (netsdk.CLIENT_ControlDeviceEx(m_hLoginHandle, NetSDKLib.CtrlType.CTRLTYPE_CTRL_CLOSE_STROBE, closeStrobe.getPointer(), null, 3000)) {
        	System.out.println("Close Success!");
        } else {
        	System.err.printf("Failed to Close 0x%x\n", netsdk.CLIENT_GetLastError());
        }
        closeStrobe.read();
	}
	
    /**
     * 手动抓图按钮事件
     */
    public static boolean manualSnapPicture(int chn) { 	
    	NetSDKLib.MANUAL_SNAP_PARAMETER snapParam = new NetSDKLib.MANUAL_SNAP_PARAMETER();
    	snapParam.nChannel = chn;
    	String sequence = "11111"; // 抓图序列号，必须用数组拷贝
    	System.arraycopy(sequence.getBytes(), 0, snapParam.bySequence, 0, sequence.getBytes().length);
    	
    	snapParam.write();
    	boolean bRet = netsdk.CLIENT_ControlDeviceEx(m_hLoginHandle, NetSDKLib.CtrlType.CTRLTYPE_MANUAL_SNAP, snapParam.getPointer(), null, 5000);
    	if (!bRet) {
    		System.err.println("Failed to manual snap, last error " + String.format("[0x%x]", netsdk.CLIENT_GetLastError()));
    		return false;
    	} else {
    		System.out.println("Seccessed to manual snap");
    	}
    	snapParam.read();
    	return true;
    }
	
    /**
     * 订阅实时上传智能分析数据
     * @return 
     */
    public static boolean attachIVSEvent(int ChannelId, NetSDKLib.fAnalyzerDataCallBack m_AnalyzerDataCB) { 	
    	/**
		 * 说明：
		 * 	通道数可以在有登录是返回的信息 m_stDeviceInfo.byChanNum 获取
		 *  下列仅订阅了0通道的智能事件.
		 */
		int bNeedPicture = 1; // 是否需要图片

        m_hAttachHandle =  netsdk.CLIENT_RealLoadPictureEx(m_hLoginHandle, ChannelId,  NetSDKLib.EVENT_IVS_ALL, 
				bNeedPicture , m_AnalyzerDataCB , null , null);
		if( m_hAttachHandle.longValue() != 0  ) {
			System.out.println("CLIENT_RealLoadPictureEx Success  ChannelId : \n" + ChannelId);
		} else {
			System.err.printf("CLIENT_RealLoadPictureEx Failed!LastError = %x\n", netsdk.CLIENT_GetLastError() );
			return false;
		}
		
		return true;
    }
    
    /**
     * 停止上传智能分析数据－图片
     */
    public static void detachIVSEvent() {
        if (0 != m_hAttachHandle.longValue()) {
        	netsdk.CLIENT_StopLoadPic(m_hAttachHandle);
            System.out.println("Stop detach IVS event");
            m_hAttachHandle.setValue(0);
        }
    }
	
	
	// 查找录像文件
	public static boolean queryRecordFile(int nChannelId, 
									   NetSDKLib.NET_TIME stTimeStart, 
									   NetSDKLib.NET_TIME stTimeEnd, 
									   NetSDKLib.NET_RECORDFILE_INFO[] stFileInfo,
									   IntByReference nFindCount) {
		// RecordFileType 录像类型 0:所有录像  1:外部报警  2:动态监测报警  3:所有报警  4:卡号查询   5:组合条件查询   6:录像位置与偏移量长度   8:按卡号查询图片(目前仅HB-U和NVS特殊型号的设备支持)  9:查询图片(目前仅HB-U和NVS特殊型号的设备支持)  10:按字段查询    15:返回网络数据结构(金桥网吧)  16:查询所有透明串数据录像文件
		int nRecordFileType = 0; 
		boolean bRet = netsdk.CLIENT_QueryRecordFile(m_hLoginHandle, nChannelId, nRecordFileType, stTimeStart, stTimeEnd, null, stFileInfo, stFileInfo.length * stFileInfo[0].size(), nFindCount, 5000, false);
		
		if(bRet) {
			System.out.println("QueryRecordFile  Succeed! \n" + "查询到的视频个数：" + nFindCount.getValue());
		} else {
			System.err.println("QueryRecordFile  Failed!" + netsdk.CLIENT_GetLastError());
			return false;
		}
		return true;
	}
	
	public static void setStreamType(int m_streamType) {
        // 设置回放时的码流类型
        IntByReference steamType = new IntByReference(m_streamType);// 0-主辅码流,1-主码流,2-辅码流
        int emType = NetSDKLib.EM_USEDEV_MODE.NET_RECORD_STREAM_TYPE;       

        boolean bret = netsdk.CLIENT_SetDeviceMode(m_hLoginHandle, emType, steamType.getPointer());
        if (!bret) {
        	System.err.printf("Set Stream Type Failed, Get last error [0x%x]\n", netsdk.CLIENT_GetLastError());
        } else {
        	System.out.println("Set Stream Type  Succeed!");
        }
	}
	
	public static NativeLong downloadRecordFile(int nChannelId,    
										     int nRecordFileType,
										     NetSDKLib.NET_TIME stTimeStart, 
										     NetSDKLib.NET_TIME stTimeEnd, 
										     String SavedFileName,
										     NetSDKLib.fTimeDownLoadPosCallBack cbTimeDownLoadPos) {
		
		m_hDownLoadHandle = netsdk.CLIENT_DownloadByTimeEx(m_hLoginHandle, nChannelId, nRecordFileType, 
															stTimeStart, stTimeEnd, SavedFileName, 
															cbTimeDownLoadPos, null, null, null, null);
		if(m_hDownLoadHandle.longValue() != 0) {
			System.out.println("Downloading RecordFile!");
		} else {
			System.err.println("Download RecordFile Failed!" + netsdk.CLIENT_GetLastError());
		}
		return m_hDownLoadHandle;
	}
	
	public static void stopDownLoadRecordFile(NativeLong m_hDownLoadHandle) {
		if (m_hDownLoadHandle.longValue() == 0) {		
			return;
		}
		netsdk.CLIENT_StopDownload(m_hDownLoadHandle);
	}
}
