package main.java.com.netsdk.demo.frame;

import java.awt.BorderLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

import main.java.com.netsdk.common.Res;
import main.java.com.netsdk.demo.panel.FunctionPanel;

/**
 * 功能列表界面
 */
public class FunctionFrame extends JFrame {
	private static final long serialVersionUID = 1L;
	
	public FunctionFrame() {
	    setTitle(Res.string().getFunctionList());
	    setSize(300, 200);
	    setLayout(new BorderLayout());
	    setLocationRelativeTo(null);
	    setResizable(false);
        	    
	    add(new FunctionPanel(this), BorderLayout.CENTER);
 
	    addWindowListener(new WindowAdapter() {
	    	public void windowClosing(WindowEvent e) {
	    		dispose();	
	    		System.exit(0);
	    	}
	    });
	}
}
