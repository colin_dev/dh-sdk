package main.java.com.netsdk.lib;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

import com.sun.jna.Pointer;
import com.sun.jna.Structure;

public class ToolKits {
  	/***************************************************************************************************
  	 *                          				工具方法       	 										   *
  	 ***************************************************************************************************/
	public static void GetPointerData(Pointer pNativeData, Structure pJavaStu)
	{
		GetPointerDataToStruct(pNativeData, 0, pJavaStu);
	}

	public static void GetPointerDataToStruct(Pointer pNativeData, long OffsetOfpNativeData, Structure pJavaStu) {
		pJavaStu.write();
		Pointer pJavaMem = pJavaStu.getPointer();
		pJavaMem.write(0, pNativeData.getByteArray(OffsetOfpNativeData, pJavaStu.size()), 0,
				pJavaStu.size());
		pJavaStu.read();
	}
	
	public static void GetPointerDataToStructArr(Pointer pNativeData, Structure []pJavaStuArr) {
		long offset = 0;
		for (int i=0; i<pJavaStuArr.length; ++i)
		{
			GetPointerDataToStruct(pNativeData, offset, pJavaStuArr[i]);
			offset += pJavaStuArr[i].size();
		}
	}
	
	/**
	 * 将结构体数组拷贝到内存
	 * @param pNativeData 
	 * @param pJavaStuArr
	 */
	public static void SetStructArrToPointerData(Structure []pJavaStuArr, Pointer pNativeData) {
		long offset = 0;
		for (int i = 0; i < pJavaStuArr.length; ++i) {
			SetStructDataToPointer(pJavaStuArr[i], pNativeData, offset);
			offset += pJavaStuArr[i].size();
		}
	}
	
	public static void SetStructDataToPointer(Structure pJavaStu, Pointer pNativeData, long OffsetOfpNativeData){
		pJavaStu.write();
		Pointer pJavaMem = pJavaStu.getPointer();
		pNativeData.write(OffsetOfpNativeData, pJavaMem.getByteArray(0, pJavaStu.size()), 0, pJavaStu.size());
	}
	
	public static void savePicture(byte[] pBuf, String sDstFile)
	{
        try
        {
          	FileOutputStream fos = new FileOutputStream(sDstFile);
        	fos.write(pBuf);
        	fos.close();	
        } catch (Exception e){
        	e.printStackTrace();
        }
	}
	
	public static void savePicture(byte[] pBuf, int dwBufOffset, int dwBufSize, String sDstFile)
	{
        try
        {       	
        	FileOutputStream fos = new FileOutputStream(sDstFile);
        	fos.write(pBuf, dwBufOffset, dwBufSize);
        	fos.close();
        } catch (Exception e){
        	e.printStackTrace();
        }
	}
	
	public static void savePicture(Pointer pBuf, int dwBufSize, String sDstFile)
	{
        try
        {
          	FileOutputStream fos = new FileOutputStream(sDstFile);
        	fos.write(pBuf.getByteArray(0, dwBufSize), 0, dwBufSize);
        	fos.close();
        } catch (Exception e){
        	e.printStackTrace();
        }
	}
	
	public static void savePicture(Pointer pBuf, int dwBufOffset, int dwBufSize, String sDstFile)
	{
        try
        {
        	FileOutputStream fos = new FileOutputStream(sDstFile);
        	fos.write(pBuf.getByteArray(dwBufOffset, dwBufSize), 0, dwBufSize);
        	fos.close();
        } catch (Exception e){
        	e.printStackTrace();
        }
	}
	
	// 将Pointer值转为byte[]
	public static String GetPointerDataToByteArr(Pointer pointer) {	
		String str = "";
		if(pointer == null) {
			return str;
		}

		int length = 0;
		byte[] bufferPlace = new byte[1];

		for(int i = 0; i < 2048; i++) {		
			pointer.read(i, bufferPlace, 0, 1);		
			if(bufferPlace[0] == '\0') {
				length = i;
				break;
			}
		}
				
		if(length > 0) {
			byte[] buffer = new byte[length];
			pointer.read(0, buffer, 0, length);
			try {
				str = new String(buffer, "GBK").trim();
			} catch (UnsupportedEncodingException e) {
				return str;
			}
		} 

		return str;
	}
	
	// 获取当前时间
	public static String getDate() {
		SimpleDateFormat simpleDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    String date = simpleDate.format(new java.util.Date()).replace(" ", "_").replace(":", "-");
	    
	    return date;
	}
	
	// 限制JTextField 长度，以及内容
	public static void limitTextFieldLength(final JTextField jTextField, final int size) {
		jTextField.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e) {			
				String number = "0123456789" + (char)8;	
				if(number.indexOf(e.getKeyChar()) < 0 || jTextField.getText().trim().length() >= size) {
					e.consume();
					return;
				}		
			}
			
			@Override
			public void keyReleased(KeyEvent e) {		
			}
			
			@Override
			public void keyPressed(KeyEvent e) {	
			}
		});
	}
	
    // 获取当前窗口
	public static JFrame getFrame(ActionEvent e) {
		JButton btn = (JButton)e.getSource();
		JFrame frame = (JFrame)btn.getRootPane().getParent();
		
		return frame;
	}
	
	// 获取操作平台信息
	public static String getLoadLibrary(String library) {
		String path = "";
		String os = System.getProperty("os.name");
		if(os.toLowerCase().startsWith("win")) {
			path = "./libs/";
		} else if(os.toLowerCase().startsWith("linux")) {
			path = "";
		}

		return (path + library);
	}
		
	public static String getOsName() {
		String osName = "";
		String os = System.getProperty("os.name");
		if(os.toLowerCase().startsWith("win")) {
			osName = "win";
		} else if(os.toLowerCase().startsWith("linux")) {
			osName = "linux";
		}
		
		return osName;
	}
}
