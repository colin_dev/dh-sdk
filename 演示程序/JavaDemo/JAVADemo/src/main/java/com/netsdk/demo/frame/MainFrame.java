package main.java.com.netsdk.demo.frame;

import java.awt.BorderLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

import main.java.com.netsdk.demo.panel.SwitchLanguagePanel;

/**
 * 选择语言界面Demo
 */
public class MainFrame extends JFrame{
	private static final long serialVersionUID = 1L;
	
	public MainFrame() {
	    setTitle("请选择语言/Please Select Language");
	    setSize(350, 200);
	    setLayout(new BorderLayout());
	    setLocationRelativeTo(null);
	    setResizable(false);

	    add(new SwitchLanguagePanel(this), BorderLayout.CENTER);
	    
	    this.addWindowListener(new WindowAdapter() {
	    	@Override
	    	public void windowClosing(WindowEvent e) {
	    		dispose(); 		
	    		System.exit(0);    		
	    	}
	    });
	}
}


