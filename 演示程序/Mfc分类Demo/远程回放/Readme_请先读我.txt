﻿【Demo功能】

1、Demo介绍SDK初始化、登陆设备、登出设备、以及录像下载和录像回放。
2、录像下载包括：按时间下载、按文件下载。Demo演示了下载，停止下载等功能
3、录像回放包括：按时间回放、按文件回放，二次压缩回放，回放时可以进行快放，慢放，暂停，恢复，前进，后退等功能。其中“二次压缩回放”即为定制项目设备配套功能（如银行项目），即一个存储设备的视频通道回放多路前端设备的码流。



【注意事项】
1、编译环境为VS2005。
2、若回放时无画面，请确认是否添加PlaySDK的DLL。
3、本demo录像回放只展示了单个设备单个通道的录像回放，若要回放多通道视频录像，请自行添加代码。
4、运行前请把"\General_NetSDK_Chn_Win32_IS_V3.XX.X.R.XXXXXX\库文件\"里所有的DLL文件复制到"\General_NetSDK_Chn_Win32_IS_V3.XX.X.R.XXXXXX\演示程序\CSharpDemo\Alarm\AlarmDemo\bin\Release\"目录中，或"\General_NetSDK_Chn_Win64_IS_V3.XX.X.R.XXXXXX\库文件\"里所有的DLL文件复制到"\General_NetSDK_Chn_Win64_IS_V3.XX.X.R.XXXXXX\演示程序\CSharpDemo\Alarm\AlarmDemo\bin\x64\Release\"目录中, 不要有遗漏DLL文件，以防启动程序时提示找不到依赖的库文件或运行出现问题。
5、如把库文件放入程序生成的目录中，运行有问题，请到大华官网下载最新的网络SDK版本：http://www.dahuatech.com/index.php/service/downloadlists/836.html 替换程序中的库文件。

【Demo Features】
1.Demo SDK initialization,login device,logout device,auto reconnect device, listen alarm information, receive alarm information, parse alarm information, stop listen alarm information function.
2.Demo external alarm, motion detection alarm,video loss alarm,camera masking alarm,HDD full alarm,HDD serror alarm.

【NOTE】
1.Complier for Demo is VS2005.
2.Just only support for listening to single device,not support for listening to multiple devices.Modify the code if the user has a requirement.
3.Just only demo gengral alarm function, add others alarm code if the user has a requirement.
4.Copy All DLL files in the directory "\General_NetSDK_Eng_Win32_IS_V3.XX.X.R.XXXXXX\bin\" into the directory "\General_NetSDK_Eng_Win32_IS_V3.XX.X.R.XXXXXX\demo\CSharpDemo\Alarm\AlarmDemo\bin\Release\", or in the directory "\General_NetSDK_Eng_Win64_IS_V3.XX.X.R.XXXXXX\bin\" into the directory "\General_NetSDK_Eng_Win64_IS_V3.XX.X.R.XXXXXX\demo\CSharpDemo\Alarm\AlarmDemo\bin\x64\Release\"  before running. To avoid prompting to cannot find the dependent DLL files when the program start, or running with some problems.
5.If run the program with some problems,please go to 
http://www.dahuasecurity.com/download_3.html download the newest version,and replace the DLL files.

