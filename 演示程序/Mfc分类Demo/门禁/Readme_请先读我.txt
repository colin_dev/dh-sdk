﻿【Demo功能】

1、Demo介绍SDK初始化、登陆设备、登出设备、门禁卡片密码操作、远程开门控制、状态查询、开门信息实时上报等功能。
2、Demo演示了门禁卡片密码增删改查的核心功能，查询开门状态，远程开门方式，实时接收开门信息上报等功能。


【注意事项】
1、编译环境为VS2005。
2、此Demo只支持单个门禁控制器。
3、一个门禁设备只能记录同一张卡一次，要修改卡片信息需要使用更新功能，或者删除卡片后重新下发。
4、运行前请把"\General_NetSDK_Chn_Win32_IS_V3.XX.X.R.XXXXXX\库文件\"里所有的DLL文件复制到"\General_NetSDK_Chn_Win32_IS_V3.XX.X.R.XXXXXX\演示程序\Mfc分类Demo\门禁\bin\x86release\"目录中，或"\General_NetSDK_Chn_Win64_IS_V3.XX.X.R.XXXXXX\库文件\"里所有的DLL文件复制到"\General_NetSDK_Chn_Win64_IS_V3.XX.X.R.XXXXXX\演示程序\Mfc分类Demo\门禁\bin\x64release\"目录中, 不要有遗漏DLL文件.。
5、如把库文件放入程序生成的目录中，运行有问题，请到大华官网下载最新的网络SDK版本：http://www.dahuatech.com/index.php/service/downloadlists/836.html 替换程序中的库文件。


【Demo Features】
1.Demo SDK initialization,login device,logout device, control the card password, open door, query status,receive real-time information about open door function.
2.Demo how to add,delete and edit the card information, query the door status,open door mode,show receive the information.

【NOTE】
1.Complier for Demo is VS2005.
2.Just only support one access control device.
3.The card can only record the same card at a time, need to use upgrade function if want change the car information,or delete the card information then record the card information.
4.Copy All DLL files in the directory "\General_NetSDK_Eng_Win32_IS_V3.XX.X.R.XXXXXX\bin\" into the directory "\General_NetSDK_Eng_Win32_IS_V3.XX.X.R.XXXXXX\demo\MfcDemo\AccessControl\bin\x86release\", or in the directory "\General_NetSDK_Eng_Win64_IS_V3.XX.X.R.XXXXXX\bin\" into the directory "\General_NetSDK_Eng_Win64_IS_V3.XX.X.R.XXXXXX\demo\MfcDemo\AccessControl\bin\x64release\"  before running.
5.If run the program with some problems,please go to 
http://www.dahuasecurity.com/download_3.html download the newest version,and replace the DLL files.