﻿【Demo功能】

1、Demo介绍SDK初始化、登陆设备、登出设备、触发网络报警输入，触发网络报警输出，设备校时，硬盘管理，设备重启，设备网络前面板。


【注意事项】
1、编译环境为VS2005。
2、此Demo只演示了设备控制的部分功能，如用户有其他需求，请自行开发。
3、运行前请把"\General_NetSDK_Chn_Win32_IS_V3.XX.X.R.XXXXXX\库文件\"里所有的DLL文件复制到"\General_NetSDK_Chn_Win32_IS_V3.XX.X.R.XXXXXX\演示程序\CSharpDemo\Alarm\AlarmDemo\bin\Release\"目录中，或"\General_NetSDK_Chn_Win64_IS_V3.XX.X.R.XXXXXX\库文件\"里所有的DLL文件复制到"\General_NetSDK_Chn_Win64_IS_V3.XX.X.R.XXXXXX\演示程序\CSharpDemo\Alarm\AlarmDemo\bin\x64\Release\"目录中, 不要有遗漏DLL文件，以防启动程序时提示找不到依赖的库文件或运行出现问题。
4、如把库文件放入程序生成的目录中，运行有问题，请到大华官网下载最新的网络SDK版本：http://www.dahuatech.com/index.php/service/downloadlists/836.html 替换程序中的库文件。

【Demo Features】
1.Demo SDK initialization,login device,logout device, trigger network alarm input,trigger network alarm input, sync device time , HDD manager, reboot device,Network Front Panel  function.

【NOTE】
1.Complier for Demo is VS2005.
2.Just only support for some general function of device control on DVR or NVR device.Modify the code if the user has a requirement.
3.Copy All DLL files in the directory "\General_NetSDK_Eng_Win32_IS_V3.XX.X.R.XXXXXX\bin\" into the directory "\General_NetSDK_Eng_Win32_IS_V3.XX.X.R.XXXXXX\demo\CSharpDemo\Alarm\AlarmDemo\bin\Release\", or in the directory "\General_NetSDK_Eng_Win64_IS_V3.XX.X.R.XXXXXX\bin\" into the directory "\General_NetSDK_Eng_Win64_IS_V3.XX.X.R.XXXXXX\demo\CSharpDemo\Alarm\AlarmDemo\bin\x64\Release\"  before running. To avoid prompting to cannot find the dependent DLL files when the program start, or running with some problems.
4.If run the program with some problems,please go to 
http://www.dahuasecurity.com/download_3.html download the newest version,and replace the DLL files.

