﻿【Demo功能】

1、Demo介绍SDK初始化、登陆设备、登出设备、自动重连、远程抓图功能。
2、Demo演示了通过选择不同的通道，抓拍选择通道的图片，保存图片及显示图片。


【注意事项】
1、编译环境为VS2005。
2、此Demo只演示单设备登陆，如用户有需求请自行修改。
4、运行前请把"\General_NetSDK_Chn_Win32_IS_V3.XX.X.R.XXXXXX\库文件\"里所有的DLL文件复制到"\General_NetSDK_Chn_Win32_IS_V3.XX.X.R.XXXXXX\演示程序\Mfc分类Demo\远程抓图\bin\x86release\"目录中，或"\General_NetSDK_Chn_Win64_IS_V3.XX.X.R.XXXXXX\库文件\"里所有的DLL文件复制到"\General_NetSDK_Chn_Win64_IS_V3.XX.X.R.XXXXXX\演示程序\Mfc分类Demo\远程抓图\bin\x64release\"目录中, 不要有遗漏DLL文件.。
5、如把库文件放入程序生成的目录中，运行有问题，请到大华官网下载最新的网络SDK版本：http://www.dahuatech.com/index.php/service/downloadlists/836.html 替换程序中的库文件。

【Demo Features】
1.Demo SDK initialization,login device,logout device,auto reconnect device, capture picture function.
2.Demo select one channel to caputre,save picture and show picture.

【NOTE】
1.Complier for Demo is VS2005.
2.Just only support one device to login.Modify the code if the user has a requirement.
3.Copy All DLL files in the directory "\General_NetSDK_Eng_Win32_IS_V3.XX.X.R.XXXXXX\bin\" into the directory "\General_NetSDK_Eng_Win32_IS_V3.XX.X.R.XXXXXX\demo\MfcDemo\CapturePicture\bin\x86release\", or in the directory "\General_NetSDK_Eng_Win64_IS_V3.XX.X.R.XXXXXX\bin\" into the directory "\General_NetSDK_Eng_Win64_IS_V3.XX.X.R.XXXXXX\demo\MfcDemo\CapturePicture\bin\x64release\"  before running.
4.If run the program with some problems,please go to 
http://www.dahuasecurity.com/download_3.html download the newest version,and replace the DLL files.

