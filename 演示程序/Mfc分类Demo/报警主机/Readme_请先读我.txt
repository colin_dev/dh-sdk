﻿【Demo功能】

1、Demo介绍SDK初始化、登陆设备、登出设备、防区配置、报警输出配置、PSTN配置、电源配置、报警中心配置、布撤防配置、各种报警联动配置等配置。防区和子系统布撤防、报警通道状态查询等功能。
2、Demo演示了各种配置获取设置的方法。报警通道布撤防，旁路，隔离等功能。报警的上报和接收，并且展示了外部报警等部分事件的上报和展示。


【注意事项】
1、编译环境为VS2005。
2、此Demo不支持多设备登陆。
3、视频预览功能只在动环主机上支持，普通报警主机不支持视频播放。
4、因报警主机型号、版本不同，可能出现部分功能设备不支持的现象。
5、运行前请把"\General_NetSDK_Chn_Win32_IS_V3.XX.X.R.XXXXXX\库文件\"里所有的DLL文件复制到"\General_NetSDK_Chn_Win32_IS_V3.XX.X.R.XXXXXX\演示程序\Mfc分类Demo\报警主机\bin\x86release\"目录中，或"\General_NetSDK_Chn_Win64_IS_V3.XX.X.R.XXXXXX\库文件\"里所有的DLL文件复制到"\General_NetSDK_Chn_Win64_IS_V3.XX.X.R.XXXXXX\演示程序\Mfc分类Demo\报警主机\bin\x64release\"目录中, 不要有遗漏DLL文件.。
6、如把库文件放入程序生成的目录中，运行有问题，请到大华官网下载最新的网络SDK版本：http://www.dahuatech.com/index.php/service/downloadlists/836.html 替换程序中的库文件。

【Demo Features】
1.Demo SDK initialization,login device,logout device, defence config,alarm out conifg,PSTN config,power or battery config, commGlobal config, others alarm config, set alarm mode, alarm sub sysetm control, alarm channel state function.
2.Demo how to get or set config, receive alarm information and show it.


【NOTE】
1.Complier for Demo is VS2005.
2.Not support multi-device to login.
3.Perview function just only support in FSU,general device is not supported.
4.Some features are not supported because of different about device version.
5.Copy All DLL files in the directory "\General_NetSDK_Eng_Win32_IS_V3.XX.X.R.XXXXXX\bin\" into the directory "\General_NetSDK_Eng_Win32_IS_V3.XX.X.R.XXXXXX\demo\MfcDemo\AlarmDevice\bin\x86release\", or in the directory "\General_NetSDK_Eng_Win64_IS_V3.XX.X.R.XXXXXX\bin\" into the directory "\General_NetSDK_Eng_Win64_IS_V3.XX.X.R.XXXXXX\demo\MfcDemo\AlarmDevice\bin\x64release\"  before running.
6.If run the program with some problems,please go to 
http://www.dahuasecurity.com/download_3.html download the newest version,and replace the DLL files.