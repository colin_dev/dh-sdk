// CHeatMapQueryDlgDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "IntelligentDevice.h"
#include "HeatMapQueryDlg.h"


// CHeatMapQueryDlgDlg 对话框






// HeatMap size 
#define HEATMAPSIZE 1024*1024*2

#define WM_HEATMAP_DISCONNECT (WM_USER + 31)

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CHeatMapQueryDlg dialog

IMPLEMENT_DYNAMIC(CHeatMapQueryDlg, CDialog)

CHeatMapQueryDlg::CHeatMapQueryDlg(CWnd* pParent /*=NULL*/)
: CDialog(CHeatMapQueryDlg::IDD, pParent)
{

	m_strPasswd = _T("admin");
	m_strUserName = _T("admin");
	m_strPort = _T("37777");
	m_oleTimeBeginDay = COleDateTime::GetCurrentTime();
	m_oleTimeBeginHour = COleDateTime::GetCurrentTime();
	m_oleTimeEndDay = COleDateTime::GetCurrentTime();
	m_oleTimeEndHour = COleDateTime::GetCurrentTime();



	m_loginID = 0;
	m_lPlayID = 0;
	m_nChannelNum = 0;
}

CHeatMapQueryDlg::~CHeatMapQueryDlg()
{

}


void CHeatMapQueryDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CBX_CHANNEL, m_cbxChannel);
	DDX_Control(pDX, IDC_IPADDRESS, m_ctlIP);
	DDX_Control(pDX, IDC_BTNLOGOUT, m_btnLogout);
	DDX_Control(pDX, IDC_BTN_QUERY, m_btnQuery);
	DDX_Control(pDX, IDC_BTN_LOGIN, m_btnLogin);
	DDX_Text(pDX, IDC_EDIT_PASSWD, m_strPasswd);
	DDX_Text(pDX, IDC_EDIT_USERNAME, m_strUserName);
	DDX_Text(pDX, IDC_EDIT_PORT, m_strPort);
	DDX_DateTimeCtrl(pDX, IDC_DATETIME_BEGIN_DAY, m_oleTimeBeginDay);
	DDX_DateTimeCtrl(pDX, IDC_DATETIME_BEIGIN_HOUR, m_oleTimeBeginHour);
	DDX_DateTimeCtrl(pDX, IDC_DATETIME_END_DAY, m_oleTimeEndDay);
	DDX_DateTimeCtrl(pDX, IDC_DATETIME_END_HOUR, m_oleTimeEndHour);
	DDX_Control(pDX, IDC_CBX_PLAN, m_cbxPlanID);
	DDX_Control(pDX, IDC_LIST_HEATMAP, m_lctHeatMap);
}


BEGIN_MESSAGE_MAP(CHeatMapQueryDlg, CDialog)
	ON_BN_CLICKED(IDC_BTN_LOGIN, &CHeatMapQueryDlg::OnBtnLogin)
	ON_BN_CLICKED(IDC_BTN_QUERY, &CHeatMapQueryDlg::OnBtnQuery)
	ON_BN_CLICKED(IDC_BTNLOGOUT, &CHeatMapQueryDlg::OnBtnlogout)
	ON_MESSAGE(WM_HEATMAP_DISCONNECT, &CHeatMapQueryDlg::OnDeviceDisConnect)
	ON_WM_DESTROY()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CHeatMapQueryDlg message handlers

BOOL CHeatMapQueryDlg::PreTranslateMessage(MSG* pMsg)
{
	// Enter key
	if(pMsg->message == WM_KEYDOWN &&
		pMsg->wParam == VK_RETURN)
	{
		return TRUE;
	}

	// Escape key
	if(pMsg->message == WM_KEYDOWN &&
		pMsg->wParam == VK_ESCAPE)
	{
		return TRUE;
	}

	return CDialog::PreTranslateMessage(pMsg);
}


void CHeatMapQueryDlg::OnBtnLogin() 
{
	// TODO: Add your control notification handler code here
	// TODO: Add your control notification handler code here
	USES_CONVERSION;
	BOOL bInvalid = UpdateData(TRUE);
	if ( FALSE == bInvalid)
	{
		return;
	}

	int err = 0;	//Storage the possible error return value.
	char *pchDVRIP;
	CString strDvrIP = GetDeviceIP();
	pchDVRIP = T2A(strDvrIP);
	WORD wDVRPort=(WORD)atoi(T2A(m_strPort));
	char *pchUserName=T2A(m_strUserName);
	char *pchPassword=T2A(m_strPasswd);

	//Call log in interface 
	NET_DEVICEINFO_Ex netDevInfo = {0};
	LLONG lRet = CLIENT_LoginEx2(pchDVRIP,wDVRPort,pchUserName,pchPassword,EM_LOGIN_SPEC_CAP_TCP,NULL,&netDevInfo,&err);
	if(0 != lRet)
	{
		m_loginID = lRet;
		m_nChannelNum = netDevInfo.nChanNum;

		/*update control state*/
		m_cbxChannel.EnableWindow(TRUE);
		m_btnLogin.EnableWindow(FALSE);
		m_btnLogout.EnableWindow(TRUE);
		m_btnQuery.EnableWindow(TRUE);

		for(int i = 0; i < m_nChannelNum; ++i)
		{
			CString tmp ;
			tmp.Format(ConvertString(_T("Channel %d")), i+1) ;
			m_cbxChannel.AddString(tmp);
			m_cbxChannel.SetItemData(i, (DWORD_PTR)i);
		}
		m_cbxChannel.SetCurSel(0);

	}
	else
	{
		ShowLoginErrorReason(err);
	}

}

void CHeatMapQueryDlg::OnBtnQuery() 
{
	// TODO: Add your control notification handler code here

	//	DH_DEVSTATE_GET_HEAT_MAP,对应结构体 NET_QUERY_HEAT_MAP


	UpdateData(TRUE);

	NET_QUERY_HEAT_MAP* pHeatMapInfo = new NET_QUERY_HEAT_MAP;
	pHeatMapInfo->dwSize = sizeof(*pHeatMapInfo);
	pHeatMapInfo->stuIn.nChannel = m_cbxChannel.GetCurSel();

	pHeatMapInfo->stuIn.nPlanID = (UINT)m_cbxPlanID.GetItemData(m_cbxPlanID.GetCurSel());
	pHeatMapInfo->stuIn.stuBegin.dwYear= m_oleTimeBeginDay.GetYear();
	pHeatMapInfo->stuIn.stuBegin.dwMonth = m_oleTimeBeginDay.GetMonth();
	pHeatMapInfo->stuIn.stuBegin.dwDay = m_oleTimeBeginDay.GetDay();
	pHeatMapInfo->stuIn.stuBegin.dwHour = m_oleTimeBeginHour.GetHour();
	pHeatMapInfo->stuIn.stuBegin.dwMinute = m_oleTimeBeginHour.GetMinute();
	pHeatMapInfo->stuIn.stuBegin.dwSecond = m_oleTimeBeginHour.GetSecond();
	pHeatMapInfo->stuIn.stuBegin.dwMillisecond = 0;

	pHeatMapInfo->stuIn.stuEnd.dwYear = m_oleTimeEndDay.GetYear();
	pHeatMapInfo->stuIn.stuEnd.dwMonth = m_oleTimeEndDay.GetMonth();
	pHeatMapInfo->stuIn.stuEnd.dwDay = m_oleTimeEndDay.GetDay();
	pHeatMapInfo->stuIn.stuEnd.dwHour = m_oleTimeEndHour.GetHour();
	pHeatMapInfo->stuIn.stuEnd.dwMinute = m_oleTimeEndHour.GetMinute();
	pHeatMapInfo->stuIn.stuEnd.dwSecond = m_oleTimeEndHour.GetSecond();
	pHeatMapInfo->stuIn.stuEnd.dwMillisecond = 0;

	pHeatMapInfo->stuOut.nBufLen = HEATMAPSIZE;
	pHeatMapInfo->stuOut.pBufData = new char[HEATMAPSIZE];


	// query heatmap 
	int pRetLen = 0;
	BOOL bRet = CLIENT_QueryDevState(m_loginID, DH_DEVSTATE_GET_HEAT_MAP,
		(char *)pHeatMapInfo,pHeatMapInfo->dwSize, &pRetLen ,3000);

	// success
	if (bRet == TRUE)
	{
		MessageBox(ConvertString(_T("Query HeatMap Info success!")),ConvertString(_T("Prompt")));
		CString text;

		CString strDataSize;
		CString strAvrage;
		CString strWidth;
		CString strHeight;
		CString strChannel;

		strChannel.Format(_T("%d"),pHeatMapInfo->stuIn.nChannel + 1 );
		strDataSize.Format(_T("%d"), pHeatMapInfo->stuOut.nBufRet);
		strAvrage.Format(_T("%d"),pHeatMapInfo->stuOut.nAverage );
		strWidth.Format(_T("%d"), pHeatMapInfo->stuOut.nWidth);
		strHeight.Format(_T("%d"),pHeatMapInfo->stuOut.nHeight );


		m_lctHeatMap.SetItemText(0,0, strChannel);
		m_lctHeatMap.SetItemText(0,1,strDataSize);
		m_lctHeatMap.SetItemText(0,2,strAvrage);
		m_lctHeatMap.SetItemText(0,3,strWidth);
		m_lctHeatMap.SetItemText(0,4,strHeight);



	}
	else
	{
		CString tmp;	
		tmp.Format(ConvertString(_T("Query HeatMap failed!")));
		MessageBox(tmp, ConvertString(_T("Prompt")));
	}

	delete[] pHeatMapInfo->stuOut.pBufData;
	delete pHeatMapInfo;


}

void CHeatMapQueryDlg::OnBtnlogout() 
{
	// TODO: Add your control notification handler code here
	// TODO: Add your control notification handler code here

	SetWindowText(ConvertString(_T("HeatMap")));
	BOOL bSuccess = CLIENT_Logout(m_loginID);

	// set login button  and logout button status
	m_btnLogin.EnableWindow(TRUE);
	m_btnLogout.EnableWindow(FALSE);
	m_btnQuery.EnableWindow(FALSE);
	m_cbxChannel.EnableWindow(FALSE);

	m_cbxChannel.Clear();
	m_cbxChannel.ResetContent();
	m_cbxChannel.EnableWindow(FALSE);

	m_btnQuery.EnableWindow(FALSE);

	for(int i =0; i < 5; i++)
	{
		m_lctHeatMap.SetItemText(0,i,_T(""));
	}

}


CString CHeatMapQueryDlg::GetDeviceIP()
{
	CString strRet=_T("");
	BYTE nField0,nField1,nField2,nField3;
	m_ctlIP.GetAddress(nField0,nField1,nField2,nField3);
	strRet.Format(_T("%d.%d.%d.%d"),nField0,nField1,nField2,nField3);
	return strRet;
}

void CHeatMapQueryDlg::InitHeatMapListCtrl()
{
	m_lctHeatMap;
	m_lctHeatMap.SetExtendedStyle(m_lctHeatMap.GetExtendedStyle()|LVS_EX_FULLROWSELECT);  
	m_lctHeatMap.SetExtendedStyle(m_lctHeatMap.GetExtendedStyle()|LVS_EX_GRIDLINES); 

	CRect rect;
	int width ;
	m_lctHeatMap.GetClientRect(&rect);
	width = rect.Width();

	LV_COLUMN lvc;
	lvc.mask=LVCF_FMT|LVCF_WIDTH|LVCF_TEXT|LVCF_SUBITEM;
	lvc.fmt=LVCFMT_LEFT;

	CString strToConvert = ConvertString(_T("Channel"));
	lvc.pszText = (LPTSTR)(LPCTSTR)strToConvert;
	lvc.cx = width/5;
	lvc.iSubItem = 0;
	m_lctHeatMap.InsertColumn(0, &lvc);

	strToConvert = ConvertString(_T("Data Size"));
	lvc.pszText = (LPTSTR)(LPCTSTR)strToConvert;
	lvc.cx = (width)/5;
	lvc.iSubItem = 1;
	m_lctHeatMap.InsertColumn(1, &lvc);

	strToConvert = ConvertString(_T("Average"));
	lvc.pszText = (LPTSTR)(LPCTSTR)strToConvert;
	lvc.cx = (width)/5;
	lvc.iSubItem = 2;
	m_lctHeatMap.InsertColumn(2, &lvc);

	strToConvert = ConvertString(_T("Width"));
	lvc.pszText = (LPTSTR)(LPCTSTR)strToConvert;
	lvc.cx = (width)/5 ;
	lvc.iSubItem = 3;
	m_lctHeatMap.InsertColumn(3, &lvc);

	strToConvert = ConvertString(_T("Height"));
	lvc.pszText = (LPTSTR)(LPCTSTR)strToConvert;
	lvc.cx = (width)/5;
	lvc.iSubItem = 4;
	m_lctHeatMap.InsertColumn(4, &lvc);


	LV_ITEM lvi;
	lvi.mask=LVIF_TEXT|LVIF_IMAGE|LVIF_PARAM;
	lvi.iSubItem = 0;
	lvi.pszText = _T("");
	lvi.iImage = 0;
	lvi.iItem = 0;


	m_lctHeatMap.InsertItem(&lvi);
}

void CHeatMapQueryDlg::ShowLoginErrorReason(int nError)
{
	if(1 == nError)		MessageBox(ConvertString(_T("Invalid password!")), ConvertString(_T("Prompt")));
	else if(2 == nError)	MessageBox(ConvertString(_T("Invalid account!")),ConvertString(_T("Prompt")));
	else if(3 == nError)	MessageBox(ConvertString(_T("Timeout!")), ConvertString(_T("Prompt")));
	else if(4 == nError)	MessageBox(ConvertString(_T("The user has logged in!")),ConvertString(_T("Prompt")));
	else if(5 == nError)	MessageBox(ConvertString(_T("The user has been locked!")), ConvertString(_T("Prompt")));
	else if(6 == nError)	MessageBox(ConvertString(_T("The user has listed into illegal!")), ConvertString(_T("Prompt")));
	else if(7 == nError)	MessageBox(ConvertString(_T("The system is busy!")),ConvertString(_T( "Prompt")));
	else if(9 == nError)	MessageBox(ConvertString(_T("You Can't find the network server!")), ConvertString(_T("Prompt")));
	else	MessageBox(ConvertString(_T("Login failed!")), ConvertString(_T("Prompt")));
}

BOOL CHeatMapQueryDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	// TODO: Add extra initialization here
	g_SetWndStaticText(this);

	InitControlData();
	InitHeatMapListCtrl();

	InitNetSDK();

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CHeatMapQueryDlg::OnDestroy() 
{
	CDialog::OnDestroy();

	// TODO: Add your message handler code here
	CLIENT_Logout(m_loginID);
	UnInitNetSDK();

}

void CHeatMapQueryDlg::InitControlData()
{
	m_ctlIP.SetAddress(10,34,3,49);

	int nIndex;
	m_cbxPlanID.ResetContent();
	for (int i = 1; i < 5; i++)
	{
		CString str;
		str.Format(ConvertString(_T("Plan %d")),i);
		nIndex = m_cbxPlanID.AddString(str);
		m_cbxPlanID.SetItemData(nIndex, (DWORD_PTR)i);
	}
	m_cbxPlanID.SetCurSel(0);

	for(int i =0; i < 5; i++)
	{
		m_lctHeatMap.SetItemText(0,i,_T(""));
	}
}

void CHeatMapQueryDlg::InitNetSDK()
{
	BOOL bSuccess = CLIENT_Init(CHeatMapQueryDlg::DisConnectFunc,(LDWORD)this);
}

void CHeatMapQueryDlg::UnInitNetSDK()
{
	CLIENT_Cleanup();
}

void  CHeatMapQueryDlg::DeviceDisConnect(LLONG lLoginID, char *sDVRIP,LONG nDVRPort)
{
	MessageBox(ConvertString(_T("Network disconnected!")), _T("Prompt"));
}


void CALLBACK CHeatMapQueryDlg::DisConnectFunc(LLONG lLoginID, char *pchDVRIP, LONG nDVRPort, LDWORD dwUser)
{
	if(0 != dwUser)
	{
		CHeatMapQueryDlg *pDlgInstance = (CHeatMapQueryDlg *)dwUser;
		HWND hwnd = pDlgInstance->GetSafeHwnd();
		::PostMessage(hwnd, WM_HEATMAP_DISCONNECT, 0 , 0);
	}

}

LRESULT CHeatMapQueryDlg::OnDeviceDisConnect(WPARAM wParam, LPARAM lParam)
{	
	SetWindowText(ConvertString(_T("Network disconnected!")));
	return 0;
}
