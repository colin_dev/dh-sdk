﻿【Demo功能】

1、Demo介绍SDK初始化、登陆设备、登出设备、自动重连、订阅智能事件、接收智能事件报警、解析智能事件信息、显示智能事件信息、停止智能事件，人流量统计，热图查询订阅功能。
2、Demo演示的智能事件包括：绊线入侵，区域入侵、物品丢失、物品遗留、人脸检测，场景变换等功能。


【注意事项】
1、编译环境为VS2005。
2、智能事件的“智能规则线”，人流量统计的的“规则线”绘制需要在web端完成，若无规则线则无法触发智能事件和实时人流量统计上报。
3、此Demo仅演示部分的智能事件的解析和智能事件通用信息展示，如需其他智能事件解析及特定信息展示，请自行添加。
4、运行前请把"\General_NetSDK_Chn_Win32_IS_V3.XX.X.R.XXXXXX\库文件\"里所有的DLL文件复制到"\General_NetSDK_Chn_Win32_IS_V3.XX.X.R.XXXXXX\演示程序\CSharpDemo\Alarm\AlarmDemo\bin\Release\"目录中，或"\General_NetSDK_Chn_Win64_IS_V3.XX.X.R.XXXXXX\库文件\"里所有的DLL文件复制到"\General_NetSDK_Chn_Win64_IS_V3.XX.X.R.XXXXXX\演示程序\CSharpDemo\Alarm\AlarmDemo\bin\x64\Release\"目录中, 不要有遗漏DLL文件，以防启动程序时提示找不到依赖的库文件或运行出现问题。
5、如把库文件放入程序生成的目录中，运行有问题，请到大华官网下载最新的网络SDK版本：http://www.dahuatech.com/index.php/service/downloadlists/836.html 替换程序中的库文件。

【Demo Features】
1.Demo SDK initialization,login device,logout device,auto reconnect device, subscribe to intelligent event information, receive intelligent event information, parse event information, unsubscribe to evnet alarm information people counting，query heatmap data.
2.Demo a part of intelligent event include Tripwire, Intrusion, Abandoned Object,Missing Object,Scene Change,Face Detection .

【NOTE】
1.Complier for Demo is VS2005.
2.before subscribe to intelligent event or subscirbe to people counting you should draw the rule line on the web ,without it the intelligent event would not notify so thant you will not receive event.
3.Just only demo gengral intelligent event  function, add others event code if you has a requirement.
4.Copy All DLL files in the directory "\General_NetSDK_Eng_Win32_IS_V3.XX.X.R.XXXXXX\bin\" into the directory "\General_NetSDK_Eng_Win32_IS_V3.XX.X.R.XXXXXX\demo\CSharpDemo\Alarm\AlarmDemo\bin\Release\", or in the directory "\General_NetSDK_Eng_Win64_IS_V3.XX.X.R.XXXXXX\bin\" into the directory "\General_NetSDK_Eng_Win64_IS_V3.XX.X.R.XXXXXX\demo\CSharpDemo\Alarm\AlarmDemo\bin\x64\Release\"  before running. To avoid prompting to cannot find the dependent DLL files when the program start, or running with some problems.
5.If run the program with some problems,please go to 
http://www.dahuasecurity.com/download_3.html download the newest version,and replace the DLL files.

