// IVSDemoDlg.h : header file
//

#pragma once


// CIVSDemoDlg dialog
class CIVSDemoDlg : public CDialog
{
// Construction
public:
	CIVSDemoDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_IVSDEMO_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

	CMenu m_Menu;
public:
	afx_msg void OnEventnotify();
	afx_msg void OnHeatmap();
	afx_msg void OnPeopleCounting();

public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
};
