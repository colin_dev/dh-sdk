#pragma once


// CCHeatMapQueryDlg 对话框

class CHeatMapQueryDlg : public CDialog
{
	DECLARE_DYNAMIC(CHeatMapQueryDlg)

public:
	CHeatMapQueryDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CHeatMapQueryDlg();

// 对话框数据
	enum { IDD = IDD_HEATMAP };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	LRESULT OnDeviceDisConnect(WPARAM wParam, LPARAM lParam);
	afx_msg void OnBtnLogin();
	afx_msg void OnBtnQuery();
	afx_msg void OnBtnlogout();
	afx_msg void OnDestroy();


protected:
	CComboBox	m_cbxChannel;
	CIPAddressCtrl	m_ctlIP;
	CButton	m_btnLogout;
	CButton	m_btnQuery;
	CButton	m_btnLogin;
	CString	m_strPasswd;
	CString	m_strUserName;
	CString	m_strPort;
	COleDateTime	m_oleTimeBeginDay;
	COleDateTime	m_oleTimeBeginHour;
	COleDateTime	m_oleTimeEndDay;
	COleDateTime	m_oleTimeEndHour;

	CComboBox m_cbxPlanID;
	CListCtrl m_lctHeatMap;

private:
	LLONG m_loginID;
	LLONG m_lPlayID;

	int m_nChannelNum;

public:
	void InitNetSDK();
	void UnInitNetSDK();

	CString GetDeviceIP();
	void	ShowLoginErrorReason(int nError);
	void	InitControlData();
	void	InitHeatMapListCtrl();

	void DeviceDisConnect(LLONG lLoginID, char *sDVRIP, LONG nDVRPort);

public:
	static void CALLBACK DisConnectFunc(LLONG lLoginID, char *pchDVRIP, LONG nDVRPort, LDWORD dwUser);

};
