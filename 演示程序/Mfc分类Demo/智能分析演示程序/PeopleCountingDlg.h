#pragma once

#include "PlayApi.h"
// CPeopleCountingDlg 对话框

class CPeopleCountingDlg : public CDialog
{
	DECLARE_DYNAMIC(CPeopleCountingDlg)

public:
	CPeopleCountingDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CPeopleCountingDlg();

// 对话框数据
	enum { IDD = IDD_PEOPLECOUNTING };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持
	virtual BOOL OnInitDialog();



	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBtnLogin();
	afx_msg void OnBtnLogout();
	afx_msg void OnBtnStartPreview();
	afx_msg void OnBtnStopPreview();
	afx_msg void OnSelchangeCbxChannel();

	afx_msg void OnDestroy();
	afx_msg void OnBtnAttachVideoSummary();
	afx_msg void OnBtnDetachVideoSummary();

	LRESULT OnVideoSummaryCome(WPARAM wParam, LPARAM lParam);
	LRESULT OnDeviceDisConnect(WPARAM wParam, LPARAM lParam);
	LRESULT OnDeviceReconnect(WPARAM wParam, LPARAM lParam);

protected:
	CComboBox	m_cbxChannel;
	CButton	m_btnStopPreview;
	CButton	m_btnStartPreview;
	CButton	m_btnLogout;
	CButton	m_btnLogin;
	CButton	m_btnDetach;
	CButton	m_btnAttach;
	CString	m_strPasswd;
	CString	m_strPort;
	CString	m_strUserName;

	CIPAddressCtrl	m_ctlIP;
	CListCtrl m_lctPeopleCounting;

private:
	LLONG m_loginID;
	LLONG m_lPlayID;
	LLONG m_lAttachID;
	int m_nChannelNum;

	CPlayAPI m_playAPI;

public:
	void InitNetSDK();
	void UnInitNetSDK();

	CString GetDeviceIP();
	void	ShowLoginErrorReason(int nError);


	void DeviceDisConnect(LLONG lLoginID, char *sDVRIP,LLONG nDVRPort);
	void ReceiveRealData(LLONG lRealHandle, DWORD dwDataType, BYTE *pBuffer, DWORD dwBufSize, LLONG lParam);

	void VideoStatSummary(NET_VIDEOSTAT_SUMMARY* pSummaryInfo);

	void InitCountingLct();
	virtual BOOL PreTranslateMessage(MSG* pMsg);

public:
	static void CALLBACK DisConnectFunc(LLONG lLoginID, char *pchDVRIP, LONG nDVRPort, LDWORD dwUser);
	static void CALLBACK RealDataCallBackEx(LLONG lRealHandle, DWORD dwDataType,
		BYTE *pBuffer,DWORD dwBufSize, LLONG lParam, LDWORD dwUser);
	static void CALLBACK HaveReConnectFunc(LLONG lLoginID, char *pchDVRIP, LONG nDVRPort, LDWORD dwUser);
	static void CALLBACK VideoStatSumCallBackFunc(LLONG lAttachHandle, NET_VIDEOSTAT_SUMMARY* pBuf, DWORD dwBufLen, LDWORD dwUser);
	
};
