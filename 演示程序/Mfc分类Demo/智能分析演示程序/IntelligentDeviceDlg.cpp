// IVSDemoDlg.cpp : implementation file
//

#include "stdafx.h"
#include "IntelligentDevice.h"
#include "IntelligentDeviceDlg.h"

#include "HeatMapQueryDlg.h"
#include "PeopleCountingDlg.h"
#include "IntelligentEventDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CIVSDemoDlg dialog




CIVSDemoDlg::CIVSDemoDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CIVSDemoDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

}

void CIVSDemoDlg::DoDataExchange(CDataExchange* pDX)
{

	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CIVSDemoDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_COMMAND(ID_EVENTNOTIFY, &CIVSDemoDlg::OnEventnotify)
	ON_COMMAND(ID_PEOPLECOUNTING, &CIVSDemoDlg::OnPeopleCounting)
	ON_COMMAND(ID_HEATMAP, &CIVSDemoDlg::OnHeatmap)
END_MESSAGE_MAP()


// CIVSDemoDlg message handlers

BOOL CIVSDemoDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	g_SetWndStaticText(this);

	m_Menu.LoadMenu(IDR_MENU_INTELLIGENT);	
	m_Menu.ModifyMenu(0,MF_BYPOSITION,ID_EVENTNOTIFY,ConvertString(_T("Event Notify")));
	m_Menu.ModifyMenu(1,MF_BYPOSITION,ID_PEOPLECOUNTING,ConvertString(_T("People Counting")));
	m_Menu.ModifyMenu(2,MF_BYPOSITION,ID_HEATMAP,ConvertString(_T("HeatMap")));
	SetMenu(&m_Menu); 	
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CIVSDemoDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CIVSDemoDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CIVSDemoDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CIVSDemoDlg::OnEventnotify()
{
	// TODO: 在此添加命令处理程序代码
	CIntelligentEventDlg dlg;
	dlg.DoModal();
}

void CIVSDemoDlg::OnHeatmap()
{
	CHeatMapQueryDlg dlg;
	dlg.DoModal();
	// TODO: 在此添加命令处理程序代码
}

void CIVSDemoDlg::OnPeopleCounting()
{
	CPeopleCountingDlg dlg;
	dlg.DoModal();
}

BOOL CIVSDemoDlg::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	if (pMsg->message == WM_KEYDOWN || pMsg->message == WM_KEYUP)
	{
		if (VK_ESCAPE == pMsg->wParam || VK_RETURN == pMsg->wParam)
		{
			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}