#pragma once

#include <list>
#include <string>
#include "PlayApi.h"
#include "afxwin.h"
#include "PictureCtrl.h"
#include "ShowPictureDlg.h"

/////////////////////////////////////////////////////////////////////////////
// CIntelligentEvent dialog
#define WM_USER_ALAMR_COME (WM_USER + 1)
#define WM_EVENT_DISCONNECT (WM_USER + 2)
#define WM_EVENT_RECONNECT (WM_USER + 3)



struct EventDataInfo
{

	std::string strGUID;
	std::string strDetail;
	std::string strTime;
	int nChannel;
	int EventID;
	DWORD index;
	BYTE* Data;
	DWORD dataSize;

};

typedef std::list< EventDataInfo > EventList;
typedef std::list< EventDataInfo* > AllEventsInfo;
typedef std::list<LLONG> handle_list;


// CIntelligentEventDlg 对话框

class CIntelligentEventDlg : public CDialog
{
	DECLARE_DYNAMIC(CIntelligentEventDlg)

public:
	CIntelligentEventDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CIntelligentEventDlg();

// 对话框数据
	enum { IDD = IDD_EVENT };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedBtnLogin();

protected:
	CListCtrl	m_lcEventInfo;
	CButton	m_btnDetachEvent;
	CButton	m_btnAttachEvent;
	CButton	m_btnStopRealPlay;
	CButton	m_btnStartRealPlay;
	CComboBox	m_cbxChannel;
	CButton	m_btnLogout;
	CButton	m_btnLogin;
	CIPAddressCtrl	m_IPCtrl;
	CString	m_strUserName;
	CString	m_strPort;
	CString	m_strPasswd;
	CComboBox	m_cbxEvent;

	CPictureCtrl m_EventPic;
	CShowPictureDlg m_dlgShowPic;


public:
	virtual BOOL OnInitDialog();
	virtual BOOL DestroyWindow();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	

protected:


	afx_msg void OnBtnLogin();
	afx_msg void OnBtnLogout();
	
	afx_msg void OnDestroy();
	afx_msg void OnBtnStartrealplay();
	afx_msg void OnBtnStoprealplay();
	afx_msg void OnBtnAttachevent();
	afx_msg void OnBtnDetachevent();
	afx_msg void OnSelchangeCbxChannel();
	afx_msg void OnCbnSelchangeCbxEvent();
	afx_msg void OnNMDblclkListEventinfo(NMHDR *pNMHDR, LRESULT *pResult);
	
	

	LRESULT OnAlarmCome(WPARAM wParam, LPARAM lParam);
	LRESULT OnDeviceDisConnect(WPARAM wParam, LPARAM lParam);
	LRESULT OnDeviceReconnect(WPARAM wParam, LPARAM lParam);

public:
	void InitNetSDK();
	void UnInitNetSDK();

	CString GetDeviceIP();
	void	ShowLoginErrorReason(int nError);
	void	InitEventListCtrl();
	void DealWithEvent(LLONG lAnalyzerHandle, DWORD dwAlarmType, char* pAlarmInfo, BYTE *pBuffer, DWORD dwBufSize, int nSequence);

	void DeviceDisConnect(LLONG lLoginID, char *sDVRIP,LLONG nDVRPort);
	void ReceiveRealData(LLONG lRealHandle, DWORD dwDataType, BYTE *pBuffer, DWORD dwBufSize, LLONG lParam);

private:
	LLONG m_loginID;
	LLONG m_lPlayID;

	handle_list m_attachList;
	CRITICAL_SECTION m_csAttachList;
	//	LLONG m_lAttachID;
	int m_nChannelNum;



	CPlayAPI m_playAPI;

	EventList m_eventList[6];
	AllEventsInfo m_allEvents;
	CRITICAL_SECTION       m_csEventList;

	DWORD m_nFilterEventType;
	DWORD m_index;



	CRITICAL_SECTION m_csLCEventInfo;


public:
	static void CALLBACK DisConnectFunc(LLONG lLoginID, char *pchDVRIP, LONG nDVRPort, LDWORD dwUser);
	static void CALLBACK HaveReConnectFunc(LLONG lLoginID, char *pchDVRIP, LONG nDVRPort, LDWORD dwUser);

	static void CALLBACK RealDataCallBackEx(LLONG lRealHandle, DWORD dwDataType, BYTE *pBuffer,DWORD dwBufSize, LLONG lParam, LDWORD dwUser);
	static int CALLBACK RealLoadPicCallback (LLONG lAnalyzerHandle, DWORD dwAlarmType, void* pAlarmInfo, 
		BYTE *pBuffer, DWORD dwBufSize, LDWORD dwUser, int nSequence, void *userdata);


private:
	static int GetAlarmInfoSize( DWORD AlarmType, DWORD *pAlarmSize);
	static int GetShowItemInfo(DWORD AlarmType,char* pAlarmInfo, std::string& strTime,int& channe, std::string& strDetail);



	bool StoreEventInfoToList(int channel, int EventID, BYTE* EventData,DWORD dataSize,
		std::string& strGUID,std::string& strTime, std::string& strDetail);

	void DisplayEventInfo(DWORD filterType, DWORD dwAlarmType, char* pAlarmInfo, BYTE *pBuffer, DWORD dwBufSize);

	void InitContorlData();

	void InitEventCombox();

	void InitEventPic();

	void DeleteAllItems();

	void DeleteAllStoreEventData();

	void EventType2Index(DWORD type, int* pIndex);

};
