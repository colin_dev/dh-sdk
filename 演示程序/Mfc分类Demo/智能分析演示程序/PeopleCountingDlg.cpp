// PeopleCounting.cpp : ʵ���ļ�
//

// VideoSummary.cpp : implementation file
//

#include "stdafx.h"
#include "IntelligentDevice.h"
#include "PeopleCountingDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPeopleCountingDlg dialog

#define PLAYPORT 450
#define WM_USER_VIDEOSUMMARY_COME (WM_USER + 11)
#define VM_VIDEOSUMMARY_DISCOONECT (WM_USER + 12)
#define WM_VIDEOSUMMARY_RECONNECT (WM_USER + 13)

IMPLEMENT_DYNAMIC(CPeopleCountingDlg, CDialog)

CPeopleCountingDlg::CPeopleCountingDlg(CWnd* pParent /*=NULL*/)
: CDialog(CPeopleCountingDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CPeopleCountingDlg)
	m_strPasswd = _T("admin");
	m_strPort = _T("37777");
	m_strUserName = _T("admin");
	//}}AFX_DATA_INIT

	m_lPlayID = 0;
	m_loginID = 0;
	m_lAttachID = 0;

	m_nChannelNum = 0;

	m_playAPI.LoadPlayDll();
}

CPeopleCountingDlg::~CPeopleCountingDlg()
{

}

void CPeopleCountingDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CBX_CHANNEL, m_cbxChannel);
	DDX_Control(pDX, IDC_BTN_STOP, m_btnStopPreview);
	DDX_Control(pDX, IDC_BTN_START, m_btnStartPreview);
	DDX_Control(pDX, IDC_BTN_LOGOUT, m_btnLogout);
	DDX_Control(pDX, IDC_BTN_LOGIN, m_btnLogin);
	DDX_Control(pDX, IDC_BTN_DETACH, m_btnDetach);
	DDX_Control(pDX, IDC_BTN_ATTACH, m_btnAttach);
	DDX_Control(pDX, IDC_IPADDRESS, m_ctlIP);
	DDX_Text(pDX, IDC_EDIT_PASSWD, m_strPasswd);
	DDX_Text(pDX, IDC_EDIT_PORT, m_strPort);
	DDX_Text(pDX, IDC_EDIT_USERNAME, m_strUserName);
	DDX_Control(pDX, IDC_LIST_COUNTING, m_lctPeopleCounting);
}



BEGIN_MESSAGE_MAP(CPeopleCountingDlg, CDialog)
	ON_BN_CLICKED(IDC_BTN_LOGIN, &CPeopleCountingDlg::OnBtnLogin)
	ON_BN_CLICKED(IDC_BTN_LOGOUT, &CPeopleCountingDlg::OnBtnLogout)
	ON_BN_CLICKED(IDC_BTN_START, &CPeopleCountingDlg::OnBtnStartPreview)
	ON_BN_CLICKED(IDC_BTN_STOP, &CPeopleCountingDlg::OnBtnStopPreview)
	ON_CBN_SELCHANGE(IDC_CBX_CHANNEL, &CPeopleCountingDlg::OnSelchangeCbxChannel)
	ON_BN_CLICKED(IDC_BTN_ATTACH, &CPeopleCountingDlg::OnBtnAttachVideoSummary)
	ON_BN_CLICKED(IDC_BTN_DETACH, &CPeopleCountingDlg::OnBtnDetachVideoSummary)
	ON_MESSAGE(WM_USER_VIDEOSUMMARY_COME, &CPeopleCountingDlg::OnVideoSummaryCome)
	ON_MESSAGE(VM_VIDEOSUMMARY_DISCOONECT, &CPeopleCountingDlg::OnDeviceDisConnect)
	ON_MESSAGE(WM_VIDEOSUMMARY_RECONNECT, &CPeopleCountingDlg::OnDeviceReconnect)
	ON_WM_DESTROY()
END_MESSAGE_MAP()



/////////////////////////////////////////////////////////////////////////////
// CPeopleCountingDlg message handlers
BOOL CPeopleCountingDlg::PreTranslateMessage(MSG* pMsg)
{
	// Enter key
	if(pMsg->message == WM_KEYDOWN &&
		pMsg->wParam == VK_RETURN)
	{
		return TRUE;
	}

	// Escape key
	if(pMsg->message == WM_KEYDOWN &&
		pMsg->wParam == VK_ESCAPE)
	{
		return TRUE;
	}

	return CDialog::PreTranslateMessage(pMsg);
}

void CPeopleCountingDlg::OnBtnLogin() 
{
	// TODO: Add your control notification handler code here
	USES_CONVERSION;
	BOOL bInvalid = UpdateData(TRUE);
	if ( FALSE == bInvalid)
	{
		return;
	}

	int err = 0;	//Storage the possible error return value.
	char *pchDVRIP;
	CString strDvrIP = GetDeviceIP();
	pchDVRIP = T2A(strDvrIP);
	WORD wDVRPort=(WORD)atoi(T2A(m_strPort));
	char *pchUserName=T2A(m_strUserName);
	char *pchPassword=T2A(m_strPasswd);

	//Call log in interface 
	NET_DEVICEINFO_Ex netDevInfo = {0};
	LLONG lRet = CLIENT_LoginEx2(pchDVRIP,wDVRPort,pchUserName,pchPassword,EM_LOGIN_SPEC_CAP_TCP,NULL,&netDevInfo,&err);
	if(0 != lRet)
	{
		m_loginID = lRet;
		m_nChannelNum = netDevInfo.nChanNum;

		/*update control state*/
		m_cbxChannel.EnableWindow(TRUE);
		m_btnLogin.EnableWindow(FALSE);
		m_btnLogout.EnableWindow(TRUE);

		m_btnStartPreview.EnableWindow(TRUE);
		m_btnStopPreview.EnableWindow(FALSE);

		m_btnAttach.EnableWindow(TRUE);
		m_btnDetach.EnableWindow(FALSE);


		for(int i = 0; i < m_nChannelNum; ++i)
		{
			CString tmp ;
			tmp.Format(ConvertString(_T("Channel %d")), i+1) ;
			m_cbxChannel.AddString(tmp);
			m_cbxChannel.SetItemData(i, (DWORD_PTR)i);
		}
		m_cbxChannel.SetCurSel(0);

		for (int j = 0; j < m_nChannelNum; ++ j)
		{
			LV_ITEM lvi;
			lvi.mask=LVIF_TEXT|LVIF_IMAGE|LVIF_PARAM;
			lvi.iSubItem = 0;
			lvi.pszText = _T("");
			lvi.iImage = 0;
			lvi.iItem = j;

			CString strChannel;
			strChannel.Format(_T("%d"),j+1);
			m_lctPeopleCounting.InsertItem(&lvi);
			m_lctPeopleCounting.SetItemText(j,0,strChannel); 
		}


		OnBtnStartPreview();
		OnBtnAttachVideoSummary();

	}
	else
	{
		ShowLoginErrorReason(err);
	}

}

void CPeopleCountingDlg::OnBtnLogout() 
{

	// TODO: Add your control notification handler code here
	SetWindowText(ConvertString(_T("People Counting")));
	OnBtnStopPreview();
	BOOL bSuccess = CLIENT_Logout(m_loginID);
	m_loginID = 0;

	// set login button  and logout button status
	m_btnLogin.EnableWindow(TRUE);
	m_btnLogout.EnableWindow(FALSE);

	// set preview button status
	m_btnStartPreview.EnableWindow(FALSE);
	m_btnStopPreview.EnableWindow(FALSE);
	m_cbxChannel.Clear();
	m_cbxChannel.ResetContent();
	m_cbxChannel.EnableWindow(FALSE);


	m_btnAttach.EnableWindow(FALSE);
	m_btnDetach.EnableWindow(FALSE);
	m_lctPeopleCounting.DeleteAllItems();
}

void CPeopleCountingDlg::OnBtnStartPreview() 
{
	// TODO: Add your control notification handler code here
	HWND hWnd = GetDlgItem(IDC_STATIC_PREVIEW)->GetSafeHwnd();
	int nSel = m_cbxChannel.GetCurSel();
	if ( (nSel < 0) || (m_loginID == 0) )
	{
		MessageBox(ConvertString(_T("Please login and select a channel before preview!")),ConvertString(_T("Prompt")));
		return;
	}
	//Open the preview
	int nChannelID =(int) m_cbxChannel.GetItemData(nSel);
	if (m_lPlayID != 0)
	{
		OnBtnStopPreview();
	}


	//Enable stream
	BOOL bOpenRet = m_playAPI.PLAY_OpenStream(PLAYPORT,0,0,1024*900);
	if(bOpenRet)
	{
		// Render intelligent data
		m_playAPI.PLAY_RenderPrivateData(PLAYPORT,1,0);

		//Begin play 
		BOOL bPlayRet = m_playAPI.PLAY_Play(PLAYPORT,hWnd);
		if(bPlayRet)
		{
			//Real-time play 
			m_lPlayID = CLIENT_RealPlayEx(m_loginID,nChannelID,0);
			if(0 != m_lPlayID)
			{
				//Callback monitor data and then save 
				CLIENT_SetRealDataCallBackEx2(m_lPlayID, CPeopleCountingDlg::RealDataCallBackEx, (LDWORD)this, 0x0f);
				m_btnStartPreview.EnableWindow(FALSE);
				m_btnStopPreview.EnableWindow(TRUE);
			}
			else
			{
				BOOL bPlay = m_playAPI.PLAY_Stop(PLAYPORT);
				if(bPlay)
				{
					//At last close PLAY_OpenStream
					BOOL bStream = m_playAPI.PLAY_CloseStream(PLAYPORT);
					m_lPlayID = 0;
				}
				MessageBox(ConvertString(_T("Fail to play!")), ConvertString(_T("Prompt")));
				m_btnStartPreview.EnableWindow(TRUE);
				m_btnStopPreview.EnableWindow(FALSE);
			}
		}
		else
		{
			MessageBox(ConvertString(_T("Open Play Stream failed!")), ConvertString(_T("Prompt")));
		}
	}

}

void CPeopleCountingDlg::OnBtnStopPreview() 
{
	// TODO: Add your control notification handler code here
	// TODO: Add your control notification handler code here
	if (m_lPlayID != 0)
	{
		BOOL bRealPlay = CLIENT_StopRealPlayEx(m_lPlayID);

		BOOL bPlay = m_playAPI.PLAY_Stop(PLAYPORT);
		if(bPlay)
		{
			//At last close PLAY_OpenStream
			BOOL bStream = m_playAPI.PLAY_CloseStream(PLAYPORT);
			m_lPlayID = 0;
		}
		//		}
	}
	m_btnStartPreview.EnableWindow(TRUE);
	m_btnStopPreview.EnableWindow(FALSE);
	Invalidate(TRUE);

}

void CPeopleCountingDlg::OnSelchangeCbxChannel() 
{
	// TODO: Add your control notification handler code here
	if (FALSE == m_btnStartPreview.IsWindowEnabled())
	{
		OnBtnStartPreview();
	}

}

CString CPeopleCountingDlg::GetDeviceIP()
{
	CString strRet=_T("");
	BYTE nField0,nField1,nField2,nField3;
	m_ctlIP.GetAddress(nField0,nField1,nField2,nField3);
	strRet.Format(_T("%d.%d.%d.%d"),nField0,nField1,nField2,nField3);
	return strRet;
}

void CPeopleCountingDlg::ShowLoginErrorReason(int nError)
{
	if(1 == nError)		MessageBox(ConvertString(_T("Invalid password!")), ConvertString(_T("Prompt")));
	else if(2 == nError)	MessageBox(ConvertString(_T("Invalid account!")),ConvertString(_T("Prompt")));
	else if(3 == nError)	MessageBox(ConvertString(_T("Timeout!")), ConvertString(_T("Prompt")));
	else if(4 == nError)	MessageBox(ConvertString(_T("The user has logged in!")),ConvertString(_T("Prompt")));
	else if(5 == nError)	MessageBox(ConvertString(_T("The user has been locked!")), ConvertString(_T("Prompt")));
	else if(6 == nError)	MessageBox(ConvertString(_T("The user has listed into illegal!")), ConvertString(_T("Prompt")));
	else if(7 == nError)	MessageBox(ConvertString(_T("The system is busy!")),ConvertString(_T( "Prompt")));
	else if(9 == nError)	MessageBox(ConvertString(_T("You Can't find the network server!")), ConvertString(_T("Prompt")));
	else	MessageBox(ConvertString(_T("Login failed!")), ConvertString(_T("Prompt")));
}

void CALL_METHOD CPeopleCountingDlg::RealDataCallBackEx(LLONG lRealHandle, DWORD dwDataType, BYTE *pBuffer,DWORD dwBufSize, LLONG lParam, LDWORD dwUser)
{
	if(dwUser == 0)
	{
		return;
	}

	CPeopleCountingDlg *dlg = (CPeopleCountingDlg *)dwUser;
	dlg->ReceiveRealData(lRealHandle,dwDataType, pBuffer, dwBufSize, lParam);
}

void CPeopleCountingDlg::InitNetSDK()
{
	BOOL bSuccess = CLIENT_Init(CPeopleCountingDlg::DisConnectFunc,(LDWORD)this);
	CLIENT_SetAutoReconnect(CPeopleCountingDlg::HaveReConnectFunc, (LDWORD)this);
}

void CPeopleCountingDlg::UnInitNetSDK()
{
	CLIENT_Cleanup();
}

//Process after receiving real-time data 
void CPeopleCountingDlg::ReceiveRealData(LLONG lRealHandle, DWORD dwDataType, BYTE *pBuffer, DWORD dwBufSize, LLONG lParam)
{
	//Stream port number according to the real-time handle.
	//Input the stream data getting from the card
	BOOL bInput=FALSE;
	if(0 != PLAYPORT)
	{
		switch(dwDataType) {
		case 0:
			//Original data 
			bInput = m_playAPI.PLAY_InputData(PLAYPORT,pBuffer,dwBufSize);
			break;
		case 1:
			//Standard video data 

			break;
		case 2:
			//yuv data 

			break;
		case 3:
			//pcm audio data 

			break;		
		default:
			break;
		}	
	}
}


void CPeopleCountingDlg::DeviceDisConnect(LLONG lLoginID, char *sDVRIP,LLONG nDVRPort)
{
	MessageBox(ConvertString(_T("Network disconnected!")), ConvertString(_T("Prompt")));
}

void CALL_METHOD CPeopleCountingDlg::DisConnectFunc(LLONG lLoginID, char *pchDVRIP, LONG nDVRPort, LDWORD dwUser)
{
	if(0 != dwUser)
	{
		CPeopleCountingDlg *pDlg = (CPeopleCountingDlg *)dwUser;
		HWND hwnd = pDlg->GetSafeHwnd();

		::PostMessage(hwnd, VM_VIDEOSUMMARY_DISCOONECT, 0, 0);
	}

}

void CALLBACK CPeopleCountingDlg::HaveReConnectFunc(LLONG lLoginID, char *pchDVRIP, LONG nDVRPort, LDWORD dwUser)
{
	if(0 != dwUser)
	{
		CPeopleCountingDlg *pDlgInstance = (CPeopleCountingDlg *)dwUser;
		HWND hwnd = pDlgInstance->GetSafeHwnd();
		::PostMessage(hwnd, WM_VIDEOSUMMARY_RECONNECT, 0, 0);		
	}

}

void CALLBACK CPeopleCountingDlg::VideoStatSumCallBackFunc(LLONG lAttachHandle, NET_VIDEOSTAT_SUMMARY* pBuf,
														   DWORD dwBufLen, LDWORD dwUser)
{
	CPeopleCountingDlg* pInstance = (CPeopleCountingDlg*)dwUser;

	NET_VIDEOSTAT_SUMMARY* pSummaryInfo = new NET_VIDEOSTAT_SUMMARY;
	memcpy(pSummaryInfo, pBuf, dwBufLen);

	HWND hwnd = pInstance->GetSafeHwnd();

	::PostMessage(hwnd, WM_USER_VIDEOSUMMARY_COME, (WPARAM)(pSummaryInfo), 0);

}

BOOL CPeopleCountingDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	// TODO: Add extra initialization here
	g_SetWndStaticText(this);

	m_ctlIP.SetAddress(10,34,3,49);
	InitCountingLct();

	InitNetSDK();

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CPeopleCountingDlg::OnDestroy() 
{
	CDialog::OnDestroy();

	// TODO: Add your message handler code here
	OnBtnStopPreview();
	CLIENT_Logout(m_loginID);
	//OnBtnLogout();
	UnInitNetSDK();
}

void CPeopleCountingDlg::InitCountingLct()
{
	m_lctPeopleCounting.SetExtendedStyle(m_lctPeopleCounting.GetExtendedStyle()|LVS_EX_FULLROWSELECT);  
	m_lctPeopleCounting.SetExtendedStyle(m_lctPeopleCounting.GetExtendedStyle()|LVS_EX_GRIDLINES); 

	CRect rect;
	int width ;
	m_lctPeopleCounting.GetClientRect(&rect);
	width = rect.Width();

	LV_COLUMN lvc;
	lvc.mask=LVCF_FMT|LVCF_WIDTH|LVCF_TEXT|LVCF_SUBITEM;
	lvc.fmt=LVCFMT_LEFT;

	CString strToConvert = ConvertString(_T("Channel"));
	lvc.pszText = (LPTSTR)(LPCTSTR)strToConvert;
	lvc.cx = width/5 - 20;
	lvc.iSubItem = 0;
	m_lctPeopleCounting.InsertColumn(0, &lvc);

	strToConvert = ConvertString(_T("Total(Enters)"));
	lvc.pszText = (LPTSTR)(LPCTSTR)strToConvert;
	lvc.cx = width/5 + 10;
	lvc.iSubItem = 1;
	m_lctPeopleCounting.InsertColumn(1, &lvc);

	strToConvert = ConvertString(_T("Today(Enters)"));
	lvc.pszText = (LPTSTR)(LPCTSTR)strToConvert;
	lvc.cx = width/5 + 10 ;
	lvc.iSubItem = 2;
	m_lctPeopleCounting.InsertColumn(2, &lvc);

	strToConvert = ConvertString(_T("Total(Exits)"));
	lvc.pszText = (LPTSTR)(LPCTSTR)strToConvert;
	lvc.cx = width/5 + 8;
	lvc.iSubItem = 3;
	m_lctPeopleCounting.InsertColumn(3, &lvc);

	strToConvert = ConvertString(_T("Today(Exits)"));
	lvc.pszText = (LPTSTR)(LPCTSTR)strToConvert;
	lvc.cx = width/5 + 8 ;
	lvc.iSubItem = 4;
	m_lctPeopleCounting.InsertColumn(4, &lvc);
}

void CPeopleCountingDlg::OnBtnAttachVideoSummary() 
{
	// TODO: Add your control notification handler code here

	if (m_loginID == 0)
	{
		MessageBox(ConvertString(_T("Please login device first!")), ConvertString(_T("Prompt")));
		return;
	}

	int tmp = -1;
	if (m_nChannelNum == 1)
	{
		tmp = 0;
	}
	NET_IN_ATTACH_VIDEOSTAT_SUM inVideoSummary;
	inVideoSummary.dwSize = sizeof(inVideoSummary);
	inVideoSummary.dwUser = (LDWORD)this;
	inVideoSummary.nChannel = tmp;
	inVideoSummary.cbVideoStatSum = CPeopleCountingDlg::VideoStatSumCallBackFunc;


	NET_OUT_ATTACH_VIDEOSTAT_SUM outVideoSummary = {sizeof(outVideoSummary)};
	LLONG lRet = CLIENT_AttachVideoStatSummary(m_loginID, &inVideoSummary, &outVideoSummary,3000);
	if (lRet == 0)
	{
		CString tmp;	
		tmp.Format(ConvertString(_T("subscribe to People Counting failed!")));
		MessageBox(tmp, ConvertString(_T("Prompt")));
		return;
	}
	m_btnAttach.EnableWindow(FALSE);
	m_btnDetach.EnableWindow(TRUE);
	m_lAttachID = lRet;

}

void CPeopleCountingDlg::OnBtnDetachVideoSummary() 
{
	// TODO: Add your control notification handler code here
	CLIENT_DetachVideoStatSummary(m_lAttachID);
	m_lAttachID = 0;

	m_btnAttach.EnableWindow(TRUE);
	m_btnDetach.EnableWindow(FALSE);
}

LRESULT CPeopleCountingDlg::OnVideoSummaryCome(WPARAM wParam, LPARAM lParam)
{
	NET_VIDEOSTAT_SUMMARY* pVideoSummaryInfo = (NET_VIDEOSTAT_SUMMARY* )wParam;

	this->VideoStatSummary(pVideoSummaryInfo);

	delete pVideoSummaryInfo;

	return 0;
}

void CPeopleCountingDlg::VideoStatSummary(NET_VIDEOSTAT_SUMMARY* pSummaryInfo)
{
	CString strChannel;
	CString strInTotal;
	CString strOutTotal;
	CString strInToday;
	CString strOutToday;

	strChannel.Format(_T("%d"), pSummaryInfo->nChannelID + 1);
	strInTotal.Format(_T("%d"),pSummaryInfo->stuEnteredSubtotal.nTotal);
	strOutTotal.Format(_T("%d"), pSummaryInfo->stuExitedSubtotal.nTotal);


	strInToday.Format(_T("%d"), 
		pSummaryInfo->stuEnteredSubtotal.nToday);

	strOutToday.Format(_T("%d"), 
		pSummaryInfo->stuExitedSubtotal.nToday
		);

	int i = pSummaryInfo->nChannelID;

	m_lctPeopleCounting.SetItemText(i, 0, strChannel);
	m_lctPeopleCounting.SetItemText(i, 1, strInTotal);
	m_lctPeopleCounting.SetItemText(i, 2, strInToday);
	m_lctPeopleCounting.SetItemText(i, 3, strOutTotal);
	m_lctPeopleCounting.SetItemText(i, 4, strOutToday);
}

LRESULT CPeopleCountingDlg::OnDeviceDisConnect(WPARAM wParam, LPARAM lParam)
{	
	SetWindowText(ConvertString(_T("Network disconnected!")));
	return 0;
}

LRESULT CPeopleCountingDlg::OnDeviceReconnect(WPARAM wParam, LPARAM lParam)
{		
	SetWindowText(ConvertString(_T("People Counting")));
	OnBtnAttachVideoSummary();
	return 0;
}