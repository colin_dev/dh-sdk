// ShowPicture.cpp : 实现文件
//

#include "stdafx.h"
#include "IntelligentDevice.h"
#include "ShowPictureDlg.h"

// CShowPictureDlg 对话框

IMPLEMENT_DYNAMIC(CShowPictureDlg, CDialog)

CShowPictureDlg::CShowPictureDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CShowPictureDlg::IDD, pParent)
{

}

CShowPictureDlg::~CShowPictureDlg()
{
}

// CShowPictureDlg 消息处理程序
void CShowPictureDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC_SHOWPIC, m_showPicture);
}


BEGIN_MESSAGE_MAP(CShowPictureDlg, CDialog)
	ON_STN_CLICKED(IDC_STATIC_SHOWPIC, &CShowPictureDlg::OnStnClickedStaticShowpic)
END_MESSAGE_MAP()


// CShowPicture 消息处理程序

void CShowPictureDlg::ShowPicture(BYTE* data, DWORD size)
{
	m_showPicture.Load(data, size);
}

void CShowPictureDlg::FreePicData()
{
	m_showPicture.FreeData();

}

void CShowPictureDlg::OnStnClickedStaticShowpic()
{
	// TODO: 在此添加控件通知处理程序代码
	ShowWindow(FALSE);
}

