//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by IVSDemo.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_IVSDEMO_DIALOG              102
#define IDR_MAINFRAME                   128
#define IDR_MENU_INTELLIGENT            129
#define IDD_EVENT                       130
#define IDD_PEOPLECOUNTING              131
#define IDD_SHOWPIC                     132
#define IDD_HEATMAP                     133
#define IDC_EDIT_PORT                   1001
#define IDC_EDIT_USERNAME               1002
#define IDC_EDIT_PASSWD                 1003
#define IDC_BUTTON1                     1004
#define IDC_LIST_EVENTINFO              1006
#define IDC_DATETIME_BEGIN_DAY          1009
#define IDC_DATETIME_BEIGIN_HOUR        1010
#define IDC_DATETIME_END_DAY            1011
#define IDC_DATETIME_END_HOUR           1012
#define IDC_BTN_QUERY                   1013
#define IDC_STATI                       1014
#define IDC_CBX_CHANNEL                 1023
#define IDC_BTN_STOP                    1024
#define IDC_CBX_EVENT                   1025
#define IDC_BTNLOGOUT                   1030
#define IDC_IPADDRESS                   1033
#define IDC_BTN_LOGIN                   1034
#define IDC_BTN_LOGOUT                  1035
#define IDC_BTN_ATTACHEVENT             1036
#define IDC_BTN_STARTREALPLAY           1044
#define IDC_BTN_STOPREALPLAY            1045
#define IDC_BTN_DETACHEVENT             1046
#define IDC_STATIC_PREVIEW              1047
#define IDC_STATIC_PICTURE              1048
#define IDC_BTN_START                   1050
#define IDC_BTN_ATTACH                  1051
#define IDC_BTN_DETACH                  1052
#define IDC_CBX_PLAN                    1054
#define IDC_STATIC_SHOWPIC              1064
#define IDC_LIST_COUNTING               1065
#define IDC_LIST_HEATMAP                1066
#define ID_EVENTNOTIFY                  32771
#define ID_HEATMAP                      32772
#define ID_PEOPLECOUNTING               32773

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        134
#define _APS_NEXT_COMMAND_VALUE         32774
#define _APS_NEXT_CONTROL_VALUE         1005
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
