﻿【Demo功能】

1、Demo介绍SDK初始化、登陆设备、登出设备、自动重连、监听报警、接收报警信息、解析报警信息、显示报警信息、报警查询功能。
2、Demo演示了外部报警、动态检测报警、视频丢失报警、视频遮挡报警、硬盘满报警、坏硬盘报警。


【注意事项】
1、编译环境为VS2005。
2、此Demo只演示监听单设备报警功能，不支持监听多设备报警功能，如用户有需求请自行修改。
3、此Demo仅演示部分通用报警功能，如需另外报警功能，请自行添加。
4、运行前请把"\General_NetSDK_Chn_Win32_IS_V3.XX.X.R.XXXXXX\库文件\"里所有的DLL文件复制到"\General_NetSDK_Chn_Win32_IS_V3.XX.X.R.XXXXXX\演示程序\Mfc分类Demo\设备报警\bin\x86release\"目录中，或"\General_NetSDK_Chn_Win64_IS_V3.XX.X.R.XXXXXX\库文件\"里所有的DLL文件复制到"\General_NetSDK_Chn_Win64_IS_V3.XX.X.R.XXXXXX\演示程序\Mfc分类Demo\设备报警\bin\x64release\"目录中, 不要有遗漏DLL文件.。
5、如把库文件放入程序生成的目录中，运行有问题，请到大华官网下载最新的网络SDK版本：http://www.dahuatech.com/index.php/service/downloadlists/836.html 替换程序中的库文件。

【Demo Features】
1.Demo SDK initialization,login device,logout device,auto reconnect device, listen alarm information, receive alarm information, parse alarm information, query alarm state function.
2.Demo external alarm, motion detection alarm,video loss alarm,camera masking alarm,HDD full alarm,HDD serror alarm.

【NOTE】
1.Complier for Demo is VS2005.
2.Just only support for listening to single device,not support for listening to multiple devices.Modify the code if the user has a requirement.
3.Just only demo gengral alarm function, add others alarm code if the user has a requirement.
4.Copy All DLL files in the directory "\General_NetSDK_Eng_Win32_IS_V3.XX.X.R.XXXXXX\bin\" into the directory "\General_NetSDK_Eng_Win32_IS_V3.XX.X.R.XXXXXX\demo\MfcDemo\Alarm\bin\x86release\", or in the directory "\General_NetSDK_Eng_Win64_IS_V3.XX.X.R.XXXXXX\bin\" into the directory "\General_NetSDK_Eng_Win64_IS_V3.XX.X.R.XXXXXX\demo\MfcDemo\Alarm\bin\x64release\"  before running.
5.If run the program with some problems,please go to 
http://www.dahuasecurity.com/download_3.html download the newest version,and replace the DLL files.

