﻿【Demo功能】
--------------------------------
1、demo演示的功能有：局域网设备搜索、跨网段设备搜索、设备初始化、修改设备IP、设备密码找回

【注意事项】
1、编译环境为VS2005。
2、跨网段搜索只支持256个IP
3、修改IP暂不支持IPv6的修改
4、对设备进行初始化和密码找回操作需要确保本机IP和设备IP是同一局域网内，如果不在同一局域网，则会报超时错误
5、如需运行demo的exe文件，请将开发包中【库文件】里的全部dll文件拷贝至该工程目录下的bin/x86release下



【Demo Features】
1.Demo demo features: LAN device search, cross-network device search, device initialization, modify the device IP, password recovery

【NOTE】
1.Complier for Demo is VS2005.
2.Cross-segment search only supports 256 IP
3.Modify the IP temporarily do not support IPv6 changes
4.Initialize the device and password recovery operations need to ensure that the local IP and device IP is the same LAN, if not in the same LAN, it will report a timeout error
5.If you want to run the demo exe file, please copy all dll files in the development package [Bin] to bin / x86release under the project 
