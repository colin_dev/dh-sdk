﻿【Demo功能】
1、Demo介绍SDK初始化、登陆设备、登出设备、电视墙配置、获取电视墙预案、载入/保存预案、电视墙预案重命名、开窗/关窗、查询/设置显示源、获取当前显示的窗口信息、查询融合屏通道信息、
查询分割能力、查询矩阵子卡信息、获取所有有效显示源、电源控制等功能。


【注意事项】
1、编译环境为VS2005。
2、此Demo只演示部分电视墙相关功能，如用户有需求请自行修改。
3、运行前请把"\General_NetSDK_Chn_Win32_IS_V3.XX.X.R.XXXXXX\库文件\"里所有的DLL文件复制到"\General_NetSDK_Chn_Win32_IS_V3.XX.X.R.XXXXXX\演示程序\CSharpDemo\Alarm\AlarmDemo\bin\Release\"目录中，或"\General_NetSDK_Chn_Win64_IS_V3.XX.X.R.XXXXXX\库文件\"里所有的DLL文件复制到"\General_NetSDK_Chn_Win64_IS_V3.XX.X.R.XXXXXX\演示程序\CSharpDemo\Alarm\AlarmDemo\bin\x64\Release\"目录中, 不要有遗漏DLL文件，以防启动程序时提示找不到依赖的库文件或运行出现问题。
4、如把库文件放入程序生成的目录中，运行有问题，请到大华官网下载最新的网络SDK版本：http://www.dahuatech.com/index.php/service/downloadlists/836.html 替换程序中的库文件。

【Demo Features】
1.Demo SDK initialization,login device,logout device, monitor wall config, get plan information of monitor wall, load or save plan, rename monitor wall's plan, open or close window,Search/set display source, get windows info,Query splice screen, Search split mode, get all valid display source,monitor wall power control.


【NOTE】
1.Complier for Demo is VS2005.
2.Just only demo gengral monitor wallfunction, add others monitorwall code if you have a requirement.
3.Copy All DLL files in the directory "\General_NetSDK_Eng_Win32_IS_V3.XX.X.R.XXXXXX\bin\" into the directory "\General_NetSDK_Eng_Win32_IS_V3.XX.X.R.XXXXXX\demo\CSharpDemo\Alarm\AlarmDemo\bin\Release\", or in the directory "\General_NetSDK_Eng_Win64_IS_V3.XX.X.R.XXXXXX\bin\" into the directory "\General_NetSDK_Eng_Win64_IS_V3.XX.X.R.XXXXXX\demo\CSharpDemo\Alarm\AlarmDemo\bin\x64\Release\"  before running. To avoid prompting to cannot find the dependent DLL files when the program start, or running with some problems.
4.If run the program with some problems,please go to 
http://www.dahuasecurity.com/download_3.html download the newest version,and replace the DLL files.

