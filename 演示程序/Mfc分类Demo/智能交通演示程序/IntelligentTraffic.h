// IntelligentTraffic.h : main header file for the IntelligentTraffic application
//

#if !defined(AFX_REALLOADPICTURE_H__D79C176B_B614_478F_A246_6DACDBE3869F__INCLUDED_)
#define AFX_REALLOADPICTURE_H__D79C176B_B614_478F_A246_6DACDBE3869F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

#include <vector>
#include <string>
#include "Utility.h"

#define CFG_SECTION_NAME			"INFO"
#define	CFG_KEY_NAME_ALARM_TYPE		"alarm_type"
#define	CFG_KEY_NAME_ALARM_INFO		"alarm_info"
#define	PC_DATA_STORE_FOLDER		"PCStore\\"
#define PC_DATA_STORE_FILE			"cfg.ini"
#define CFG_CFG_FILE_NAME			"Config.ini"
#define CFG_SECTION_CFG				"CFG"
#define CFG_KEY_DATA_PATH			"DATA_PATH"
#define MAX_GUID_LEN 55

#define GET_STRUCT_SIZE			0x01
#define GET_EVENT_CONTENT		0x02
#define GET_DEVICE_ADDRESS		0x04
#define GET_PICTURE_RECT		0x08
#define GET_PLATE_PICTURE_INFO	0x10

const COLORREF RESERVED_COLOR = RGB(255, 255, 255);

typedef struct __COL_DES
{
	std::string strColTitle; // title of column
	int			nColWidth;   // width of column
}ColDes;


struct EVENT_INFO_DISPLAY
{
	std::string strCountNum;			// count num
	std::string strTime;				// time
	std::string strEventType;			// event type

	std::string strFileIndex;			// file index
	std::string strFileCount;			// file count
	std::string strGroupId;				// group id

	std::string strPlateNumber;			// plate text 
	std::string strPlateColor;			// plate color
	std::string strPlateType;			// plate type

	std::string	strVehicleType;			// vehicle type
	std::string strVehicleColor;		// vehicle color
	std::string strVehicleSize;			// vehicle size

	std::string strLane;				// lane number	
	std::string strDeviceAddress;		// device address
};

void  g_SetWndStaticText(CWnd * pWnd);

std::string GetDataFolder();
BOOL IsTypeHasLP(DWORD dwEventType);


class CRealLoadPictureApp : public CWinApp
{
public:
	CRealLoadPictureApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRealLoadPictureApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CRealLoadPictureApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////
inline ColDes ColDesObj(char* szColTitle, const int nColWidth)
{
	ColDes colDes;
	colDes.strColTitle = ConvertStr(szColTitle);
	colDes.nColWidth = nColWidth;
	return colDes;
}

struct StuEventInfo
{
	int					nStructSize;			// event struct size
	EVENT_INFO_DISPLAY  stuEventInfoToDisplay;	// Event info to display
	std::string			strDeviceAddress;		// event address
	DH_RECT				rectBoundingBox;		
	DWORD				dwOffset;
	DWORD				dwOffsetLength;
	std::string			strUTCTime;

	StuEventInfo() : nStructSize(0), strDeviceAddress(""), dwOffset(0), dwOffsetLength(0), strUTCTime("")
	{		
		rectBoundingBox.bottom = 0;
		rectBoundingBox.left = 0;
		rectBoundingBox.right = 0;
		rectBoundingBox.top = 0;
	}
};

BOOL GetStructInfo(DWORD nEventType, void* pEventInfo,int nGetWhat, StuEventInfo& stuEventInfo);
void TraceOut (const char * szFmt, ...);
std::string EventType2Str(int nEventType);


//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.


#endif // !defined(AFX_REALLOADPICTURE_H__D79C176B_B614_478F_A246_6DACDBE3869F__INCLUDED_)
