﻿【Demo功能】
1、Demo介绍了SDK初始化、登陆设备、登出设备、设备断线重连、实时监视、音频输入配置、通道名称配置、背光配置、白平衡配置、曝光属性配置、透雾配置，聚焦信息配置、图像属性配置、视频输入颜色配置、白夜模式配置、防抖配置、切换模式配置、锐度配置、图像增强配置、补光灯配置、自动光圈配置、视频降噪配置等功能。


【注意事项】
1、编译环境为VS2005。
2、此Demo登录设备后自动播放通道1的实时监视信息，切换通道时，自动播放相应通道实时监视信息。
3、运行前请把压缩包内"General_NetSDK_Chn_WinXXX_IS_VXXX.R.XXX.7z\库文件"里所有的DLL文件复制到"\摄像头属性\bin"对应的生成目录中，不要有遗漏DLL文件，以防启动程序时提示找不到依赖的库文件。
4、如把库文件放入程序生成的目录中，运行有问题，请到大华官网下载最新的网络SDK版本：http://www.dahuatech.com/index.php/service/downloadlists/836.html 替换程序中的库文件。

【Demo Features】
1.Demo SDK initialization,login device, logout device, auto reconnect device, real play, audio input config, channel name config, backlight config, white balance config, exposure config, penetrating fog config, focused info config, image attributes config, color config of video input, day-night mode config, anti-shake config, switch mode config, sharpness config, image enhancement config, fill light config, auto-iris config, video denoise config.

【NOTE】
1.Complier for Demo is VS2005.
2.Plays the real-time monitoring information of channel 1 after logging in and automatically plays the real-time monitoring information of the corresponding channel when switching channels.
3.Copy All DLL files in the "General_NetSDK_Chn_WinXXX_IS_VXXX.R.XXX.7z/bin" directory into the directory where the program is built,that in the "\ImageTest\bin" path , before running. To avoid prompting to cannot find the dependent DLL files when the program start.
4.If run the program with some problems,please go to http://www.dahuasecurity.com/download_3.html download the newest version,and replace the DLL files.

