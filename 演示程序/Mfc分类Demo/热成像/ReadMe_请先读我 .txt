﻿【Demo功能】
1、Demo介绍了SDK初始化、登陆设备、登出设备、设备断线重连、实时监视、查询预置信息、查询测温点信息、查询测温信息、订阅报警信息、停止订阅报警信息、订阅热图信息、停止订阅热图信息、获取热图信息等功能。
2. Demo演示了实时监视、预置信息、测温点信息、测温信息、热成像测温点温度报警信息及热图信息。


【注意事项】
1、编译环境为VS2005。
2、此Demo提供两种方式展示实时监视信息：直接播放方式及数据回调方式。
3、此Demo订阅所有报警信息，但只显示热成像测温点温度异常报警事件，如需另外报警功能，请自行添加。
4、运行前请把压缩包内"General_NetSDK_Chn_WinXXX_IS_VXXX.R.XXX.7z\库文件"里所有的DLL文件复制到"\热成像\bin"对应的生成目录中，不要有遗漏DLL文件，以防启动程序时提示找不到依赖的库文件。
5、如把库文件放入程序生成的目录中，运行有问题，请到大华官网下载最新的网络SDK版本：http://www.dahuatech.com/index.php/service/downloadlists/836.html 替换程序中的库文件。

【Demo Features】
1.Demo SDK initialization,login device, logout device, auto reconnect device, real play, preset info, thermal point info ,listen alarm information, stop listen alarm information, subscrible thermal info, get thermal function.
2.Demo displays real play, preset info, thermal info, thermal item info, thermal temperature abnormal event alarm, heatmap info.

【NOTE】
1.Complier for Demo is VS2005.
2.Supports two way to display real play info: direct play and data-callback play.
3.Subscribles all the alarm info, and only display thermal temperature abnormal event alarm.
4.Copy All DLL files in the "General_NetSDK_Chn_WinXXX_IS_VXXX.R.XXX.7z/bin" directory into the directory where the program is built,that in the "\ThermalCamera\bin" path , before running. To avoid prompting to cannot find the dependent DLL files when the program start.
5.If run the program with some problems,please go to 
http://www.dahuasecurity.com/download_3.html download the newest version,and replace the DLL files.

