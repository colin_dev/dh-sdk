// Heatmap.cpp : implementation file
//

#include "stdafx.h"
#include "ThermalCamera.h"
#include "Heatmap.h"
#include "ThermalCameraDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CHeatmap dialog


CHeatmap::CHeatmap(CWnd* pParent /*=NULL*/,LLONG lLoginId,int nChannel)
	: CDialog(CHeatmap::IDD, pParent)
{
	//{{AFX_DATA_INIT(CHeatmap)
	m_strStatus = _T("");
	m_nChannel = 0;
	m_nHeight = 0;
	m_nLength = 0;
	m_StrSensorType = _T("");
	m_StrTime = _T("");
	m_nWidth = 0;
	//}}AFX_DATA_INIT
    m_lLoginID = lLoginId;
    m_lAttachhandle = 0;
    m_nHeatChannel = nChannel;
}


void CHeatmap::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CHeatmap)
	DDX_Control(pDX, IDC_STOP, m_BtnStop);
	DDX_Control(pDX, IDC_STARTFETCH, m_BtnStart);
	DDX_Control(pDX, IDC_ATTACH, m_BtnAttach);
	DDX_Text(pDX, IDC_EDIT_STATUS, m_strStatus);
	DDX_Text(pDX, IDC_EDIT_CHANNEL, m_nChannel);
	DDX_Text(pDX, IDC_EDIT_HEIGHT, m_nHeight);
	DDX_Text(pDX, IDC_EDIT_LENGTH, m_nLength);
	DDX_Text(pDX, IDC_EDIT_SENSORTYPE, m_StrSensorType);
	DDX_Text(pDX, IDC_EDIT_TIME, m_StrTime);
	DDX_Text(pDX, IDC_EDIT_WIDTH, m_nWidth);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CHeatmap, CDialog)
	//{{AFX_MSG_MAP(CHeatmap)
	ON_BN_CLICKED(IDC_STARTFETCH, OnStartfetch)
	ON_BN_CLICKED(IDC_ATTACH, OnAttach)
	ON_BN_CLICKED(IDC_STOP, OnStop)
    ON_MESSAGE(WM_HEATMAPINFO, OnShowInfo)
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CHeatmap message handlers
LRESULT CHeatmap::OnShowInfo(WPARAM wParam, LPARAM lParam)
{
    NET_RADIOMETRY_DATA *pBuf = (NET_RADIOMETRY_DATA*)wParam;
    m_nHeight = pBuf->stMetaData.nHeight;
    m_nWidth = pBuf->stMetaData.nWidth;
    m_nChannel = pBuf->stMetaData.nChannel;
    m_nLength = pBuf->stMetaData.nLength;
    m_StrSensorType = pBuf->stMetaData.szSensorType;
    m_StrTime.Format("%2d-%d-%d-%d-%d-%d",pBuf->stMetaData.stTime.dwYear,pBuf->stMetaData.stTime.dwMonth,
        pBuf->stMetaData.stTime.dwDay, pBuf->stMetaData.stTime.dwHour,pBuf->stMetaData.stTime.dwMinute,
        pBuf->stMetaData.stTime.dwSecond);
    UpdateData(FALSE);
    return 0;
}
void CALLBACK  cbRadiometryAttachCB(LLONG  lAttachHandle,  NET_RADIOMETRY_DATA* pBuf, int nBufLen, LDWORD dwUser) 							
{
    //PRINT(pBuf->stMetaData.nLength);
    TRACE("热图大小：%d\n",pBuf->stMetaData.nLength);
    
    
    TRACE("回调热图数据完毕\n");
    CHeatmap *dlg = (CHeatmap *)dwUser;
    if ( dlg!=NULL)
    {
        dlg->PostMessage(WM_HEATMAPINFO, (WPARAM)pBuf, (LPARAM)0);
    }
   /* UpdateData(FALSE);*/
}

BOOL CHeatmap::OnInitDialog() 
{
	CDialog::OnInitDialog();
    g_SetWndStaticText(this);
	m_BtnStart.EnableWindow(FALSE);
    m_BtnStop.EnableWindow(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CHeatmap::OnStartfetch() 
{
    NET_IN_RADIOMETRY_FETCH stInFetch = {sizeof(stInFetch), m_nHeatChannel};
    NET_OUT_RADIOMETRY_FETCH stOutFetch = {sizeof(stOutFetch)};
    CLIENT_RadiometryFetch(m_lLoginID, &stInFetch, &stOutFetch, 1000);
    if (stOutFetch.nStatus ==0)
    {
        m_strStatus = ConvertString("Unknown");
    }
    else if (stOutFetch.nStatus == 1)
    {
        m_strStatus = ConvertString("free");
    }
    else if (stOutFetch.nStatus == 2)
    {
        m_strStatus = ConvertString("acquiring");
    }
 //   m_BtnStart.EnableWindow(FALSE);
    //PRINT(stOutFetch.nStatus);
	TRACE("stOutFetch status=%d\n",stOutFetch.nStatus);
    UpdateData(FALSE);
}

void CHeatmap::OnAttach() 
{
    NET_IN_RADIOMETRY_ATTACH stIn = {sizeof(stIn)};
    stIn.nChannel = m_nHeatChannel;
    stIn.dwUser = (LDWORD)this;
    stIn.cbNotify = cbRadiometryAttachCB;
    NET_OUT_RADIOMETRY_ATTACH stOut = {sizeof(stOut)};
    m_lAttachhandle = CLIENT_RadiometryAttach(m_lLoginID, &stIn, &stOut, 1000);
    if (!m_lAttachhandle)
    {
        TRACE("CLIENT_RadiometryAttach\n");
    }
    m_BtnAttach.EnableWindow(FALSE);
    m_BtnStart.EnableWindow(TRUE);
    m_BtnStop.EnableWindow(TRUE);
}

void CHeatmap::OnStop() 
{
	CLIENT_RadiometryDetach(m_lAttachhandle);
    TRACE("CLIENT_RadiometryDetach\n");
    m_BtnAttach.EnableWindow(TRUE);
    m_BtnStart.EnableWindow(FALSE);
    m_BtnStop.EnableWindow(FALSE);
}

void CHeatmap::OnDestroy() 
{
	CDialog::OnDestroy();
    CLIENT_RadiometryDetach(m_lAttachhandle);
    TRACE("CLIENT_RadiometryDetach\n");
	
}
