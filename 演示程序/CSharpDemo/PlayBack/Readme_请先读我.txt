﻿【Demo功能】

1、Demo介绍SDK初始化、登陆设备、登出设备、按时间(一天时间)回放录像、按时间下载录像功能，快放，慢放、暂停，停止等功能。
2、Demo演示了在回放或下载前可选择通道、码流类型。
3、Demo中带有一天时间录像的显示控件。


【注意事项】
1、编译环境为VS2010，NETSDKCS库最低只支持.NET Framework 4.0,如用户需要支持低于4.0的版本需要更改NetSDK.cs文件中使用到IntPtr.Add的方法,我们不提供修改。
2、此Demo不支持超一天的回放，如用户需要多天的录像回放，请自行修改及相应的显示控件。
3、此Demo只支持单通道录像回放及下载功能。
4、此Demo不支持多设备登陆。
5、运行前请把"\General_NetSDK_Chn_Win32_IS_V3.XX.X.R.XXXXXX\库文件\"里所有的DLL文件复制到"\General_NetSDK_Chn_Win32_IS_V3.XX.X.R.XXXXXX\演示程序\CSharpDemo\PlayBack\PlayBackAndDownloadDemo\bin\Release\"目录中，或"\General_NetSDK_Chn_Win64_IS_V3.XX.X.R.XXXXXX\库文件\"里所有的DLL文件复制到"\General_NetSDK_Chn_Win64_IS_V3.XX.X.R.XXXXXX\演示程\CSharpDemo\PlayBack\PlayBackAndDownloadDemo\bin\x64\Release\"目录中, 不要有遗漏DLL文件，以防启动程序时提示找不到依赖的库文件或运行出现问题。
6、如把库文件放入程序生成的目录中，运行有问题，请到大华官网下载最新的网络SDK版本：http://www.dahuatech.com/index.php/service/downloadlists/836.html 替换程序中的库文件。

【Demo Features】
1、Demo SDK initialization,login device,logout device,playback by time(one day),download recoder file by time,fast play,slow play, pause,stop play function.
2、Demo can select channel and stream type before playback or download.
3、Demo use an one day control for show playback.

【NOTE】
1、Complier for NetSDKCS project and Demo is VS2010,and target framework for NetSDKCS project is .NET Framework 4.Modify the code about IntPtr.Add method in the file "NetSDK.cs"，we don not support to modify it.
2、Not support over one day to playback,modify the code if the user has a requirement.
3、Just only support one channel to playback or download.
4、Not support multi-device to login.
5、Copy All DLL files in the directory "\General_NetSDK_Eng_Win32_IS_V3.XX.X.R.XXXXXX\bin\" into the directory "\General_NetSDK_Eng_Win32_IS_V3.XX.X.R.XXXXXX\demo\CSharpDemo\PlayBack\PlayBackAndDownloadDemo\bin\Release\", or in the directory "\General_NetSDK_Eng_Win64_IS_V3.XX.X.R.XXXXXX\bin\" into the directory "\General_NetSDK_Eng_Win64_IS_V3.XX.X.R.XXXXXX\demo\CSharpDemo\PlayBack\PlayBackAndDownloadDemo\bin\x64\Release\"  before running. To avoid prompting to cannot find the dependent DLL files when the program start, or running with some problems.
6、If run the program with some problems,please go to 
http://www.dahuasecurity.com/download_3.html download the newest version,and replace the DLL files.