﻿【Demo功能】

1、Demo介绍SDK初始化、登陆设备、登出设备、开始对讲，关闭对讲功能。
2、Demo演示可直连设备和设备转发二种模式。


【注意事项】
1、编译环境为VS2010，NETSDKCS库最低只支持.NET Framework 4.0,如用户需要支持低于4.0的版本需要更改NetSDK.cs文件中使用到IntPtr.Add的方法,我们不提供修改。
2、此Demo不支持多设备登陆。
3、此Demo只支持大华二代协议对讲。
4、设备转发模式是指通过客户端与前端设备对讲，客户端没有与前端设备直连，客户端与存储设备直连，存储设备与前端设备连接，对讲的时候语音数据通过存储设备转发给前端设备，实现客户端与前端设备对讲的模式。
5、运行前请把"\General_NetSDK_Chn_Win32_IS_V3.XX.X.R.XXXXXX\库文件\"里所有的DLL文件复制到"\General_NetSDK_Chn_Win32_IS_V3.XX.X.R.XXXXXX\演示程序\CSharpDemo\Talk\TalkDemo\bin\Release\"目录中，或"\General_NetSDK_Chn_Win64_IS_V3.XX.X.R.XXXXXX\库文件\"里所有的DLL文件复制到"\General_NetSDK_Chn_Win64_IS_V3.XX.X.R.XXXXXX\演示程\CSharpDemo\Talk\TalkDemo\bin\x64\Release\"目录中, 不要有遗漏DLL文件，以防启动程序时提示找不到依赖的库文件或运行出现问题。
6、如把库文件放入程序生成的目录中，运行有问题，请到大华官网下载最新的网络SDK版本：http://www.dahuatech.com/index.php/service/downloadlists/836.html 替换程序中的库文件。

【Demo Features】
1、Demo SDK initialization,login device,logout device,start talk,stop talk function.
2、Demo can select direct connect device mode or device transfer mode.


【NOTE】
1、Complier for NetSDKCS project and Demo is VS2010,and target framework for NetSDKCS project is .NET Framework 4.Modify the code about IntPtr.Add method in the file "NetSDK.cs"，we don not support to modify it.
2、Not support multi-device to login.
3、Just only support DH-2 protocol for talk.
4、device transfer mode means such as client talk with IPC,but client donot connect to IPC, client connect to NVR,NVR connect with IPC, NVR device transfer talk data to IPC when client tlak with IPC.
5、Copy All DLL files in the directory "\General_NetSDK_Eng_Win32_IS_V3.XX.X.R.XXXXXX\bin\" into the directory "\General_NetSDK_Eng_Win32_IS_V3.XX.X.R.XXXXXX\demo\CSharpDemo\Talk\TalkDemo\bin\Release\", or in the directory "\General_NetSDK_Eng_Win64_IS_V3.XX.X.R.XXXXXX\bin\" into the directory "\General_NetSDK_Eng_Win64_IS_V3.XX.X.R.XXXXXX\demo\CSharpDemo\Talk\TalkDemo\bin\x64\Release\"  before running. To avoid prompting to cannot find the dependent DLL files when the program start, or running with some problems.
6、If run the program with some problems,please go to 
http://www.dahuasecurity.com/download_3.html download the newest version,and replace the DLL files.